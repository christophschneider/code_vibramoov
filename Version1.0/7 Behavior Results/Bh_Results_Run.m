%BH_RESULTS_RUN - Analyzes the subjective reports regarding the experienced
%illusions
%
% Syntax:  Bh_Results_Run()
%
% Inputs:
%    none
%
% Outputs:
%    none
%
% Notes:
%    Select subject folders in the UI, then click 'add' and 'done'.
%
% Other m-files required: Bh_Results_Settings.m, Bh_Results_Main.m
% Subfunctions: none
% External files required: sequences files
%
% Author: Christoph Schneider
% Acute Neurorehabilitation Unit (LRNA)
% Division of Neurology, Department of Clinical Neurosciences
% Centre Hospitalier Universitaire Vaudois (CHUV)
% Rue du Bugnon 46, CH-1011 Lausanne, Switzerland
%
% email: christoph.schneider.phd@gmail.com
% March 2021
%------------- BEGIN CODE --------------

clear;
close all force;
clc;

% Load defaults
% -------------------------------------------------------------------------
Bh_Results_Settings()

% Select folders
% -------------------------------------------------------------------------
file = uipickfiles('Prompt','Select Folders','FilterSpec',default.path.clsfres);
load(file{1})

% make average vectors and summaries
% -------------------------------------------------------------------------
subj = fields(trialsummary);
A_means = nan(length(subj),7);

nanzscore = @(x) bsxfun(@rdivide, bsxfun(@minus, x, mean(x,'omitnan')), std(x, 'omitnan'));

p_SingleSubj.rating = array2table(zeros(1,4),'VariableNames',{'subject','diff70_90','diff70_0','diff90_0'});
p_SingleSubj.reaction = array2table(zeros(1,2),'VariableNames',{'subject','diff70_90'});
p_SingleSubj.angles = array2table(zeros(1,4),'VariableNames',{'subject','diff70_90','diff70_0','diff90_0'});

for s = 1:length(subj)
    
    sn = s;
    %sn = str2double(subj{s}(end-1:end));
    
    % summarize and z-score ratings and reaction times
    % -------------------------------------------------------------------------
    trialcond = cat(1, trialsummary.(subj{s}){1}(:,2), trialsummary.(subj{s}){2}(:,2));
    trialrating = nanzscore( cat(1, trialsummary.(subj{s}){1}(:,3), trialsummary.(subj{s}){2}(:,3)) );
    trialreact = cat(1, trialsummary.(subj{s}){1}(:,4), trialsummary.(subj{s}){2}(:,4));
    
    A_means(sn,1) = sn;
    A_means(sn,2) = nanmean(trialrating(trialcond==70));
    A_means(sn,3) = nanmean(trialrating(trialcond==90));
    A_means(sn,4) = nanmean(trialreact(trialcond==70));
    A_means(sn,5) = nanmean(trialreact(trialcond==90));
    
    trialcond = trialsummary.(subj{s}){3}(:,2);
    trialangle = trialsummary.(subj{s}){3}(:,3);
    
    A_means(sn,6) = nanmean(trialangle(trialcond==70));
    A_means(sn,7) = nanmean(trialangle(trialcond==90));
    
    % summarize p-values
    % -------------------------------------------------------------------------
    p_SingleSubj.rating(sn,:) = [array2table(sn), struct2table(p_struct.(subj{s}).rating)];
    p_SingleSubj.reaction(sn,:) = [array2table(sn), struct2table(p_struct.(subj{s}).reaction)];
    p_SingleSubj.angles(sn,:) = [array2table(sn), struct2table(p_struct.(subj{s}).angles)];
    
end

T_means = array2table(A_means,'VariableNames',{'subject','rating_70','rating_90','reaction_70','reaction_90','angle_70','angle_90'});


%% Grand average testing

% ratings
% -------------------------------------------------------------------------
[~, p.ratings,~,stats.ratings] = ttest(T_means.rating_70, T_means.rating_90);

% reactions
% -------------------------------------------------------------------------
[~, p.reaction,~,stats.reaction] = ttest(T_means.reaction_70, T_means.reaction_90);

% angles
% -------------------------------------------------------------------------
[~, p.angles,~,stats.angles] = ttest(T_means.angle_70, T_means.angle_90);
[~, p.angles70to0,~,stats.angles70to0] = ttest(T_means.angle_70);
[~, p.angles90to0,~,stats.angles90to0] = ttest(T_means.angle_90);


%% Grand average plots

% ratings
% -------------------------------------------------------------------------

figure;
subplot(1,3,1)
boxplot([T_means.rating_70, T_means.rating_90],'Notch','on','ColorGroup',[1,2])
xticklabels({'Control','Illusion'})
ylabel('mean z-scored subjective ratings')
title('Grand Average subjective rating')
sigstar({[1,2]},p.ratings,0)
set(gca,'FontSize',16);

% reactions
% -------------------------------------------------------------------------

subplot(1,3,2)
boxplot([T_means.reaction_70, T_means.reaction_90],'Notch','on','ColorGroup',[1,2])
xticklabels({'Control','Illusion'})
ylabel('mean time of illusion onset [ms]')
title('Grand Average movement onset time')
sigstar({[1,2]},p.reaction,0)
set(gca,'FontSize',16);

% angles
% -------------------------------------------------------------------------

subplot(1,3,3)
boxplot([T_means.angle_70, T_means.angle_90],'Notch','on','ColorGroup',[1,2])
xticklabels({'Control','Illusion'})
ylabel('mean experienced elbow rotation [deg]')
title('Grand Average movement mirroring')
sigstar({[1,2]},p.angles,0)
set(gca,'FontSize',16);


disp('---> Batch file processing end <---')