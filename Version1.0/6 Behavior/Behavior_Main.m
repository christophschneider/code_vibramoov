function [] = Behavior_Main(args)
%BEHAVIOR_MAIN - Extracts and summarizes subjective rating data
% Specifically:
% - Extracts data from behavioral log files
% - Computes key parameters of the personal rating per class and in common
% - Saves a struct in a combined file
%
% Syntax:  [] = Results_Plots(args)
%
% Inputs:
%    args (struct) - arguments from Behavior_Run.m and Behavior_Settings.m
%
% Outputs:
%    none
%
% Example:
%    [] = Behavior_Main(args)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% Author: Christoph Schneider
% Acute Neurorehabilitation Unit (LRNA)
% Division of Neurology, Department of Clinical Neurosciences
% Centre Hospitalier Universitaire Vaudois (CHUV)
% Rue du Bugnon 46, CH-1011 Lausanne, Switzerland
%
% email: christoph.schneider.phd@gmail.com
% March 2021
%------------- BEGIN CODE --------------

Behavior_Settings();

% iterate across recording files
for f = 1:length(args.filenames)
    fn = fullfile(args.filenames(f).folder, args.filenames(f).name);
    rt = readtable(fn,'Filetype', 'text','Format','%f%s');
    
    % only keep events in trials
    tt = num2cell(rt.time(contains(rt.event,'trial')));
    ct = cell2table([tt, split(rt.event(contains(rt.event,'trial')))]);
    ct.Properties.VariableNames = {'time','descriptor','trialnum','event','rating'}; % names of columns
    ct.trialnum = str2double(ct.trialnum);
    trialnumbers = unique(ct.trialnum);
    
    %% Extract trial conditions from Vibration protocol files
    
    % load predefined stimulation sequence for this recording
    [discardpath, type, ~] = fileparts(args.filenames(f).folder);
    [discardpath, ~, ~] = fileparts(discardpath);
    [~, subject, ~] = fileparts(discardpath);
    subject = strrep(subject,'-','_');
    run = str2double(args.filenames(f).name([4,5] + strfind(args.filenames(f).name,'run-')));
    seq = readtable(fullfile(default.path.sequences,type,['Block',num2str(run),'.txt']));
    vib0 = seq.Out_5;
    
    % find peaks in raw trigger channel
    [condition, ~] = findpeaks(vib0, 'MinPeakDistance',5, 'MinPeakHeight',40);
    
    
    %% create trialsummary (number, condition, rating, reaction time)
    for trial = trialnumbers'
        
        trialsummary.(subject){f}(trial,1) = trial;
        trialsummary.(subject){f}(trial,2) = condition(trial);
        rating = str2double( ct.rating(ct.trialnum == trial & strcmp(ct.event,'rating'),:) );
        if isempty(rating)
            rating = NaN;
        end
        trialsummary.(subject){f}(trial,3) = rating;
        time_start = ct.time(ct.trialnum == trial & strcmp(ct.event,'start'),:);
        time_end = ct.time(ct.trialnum == trial & strcmp(ct.event,'reaction'),:);
        bool_react = str2double( ct.rating(ct.trialnum == trial & strcmp(ct.event,'reaction'),:) );
        if isempty(bool_react) || bool_react==0
            time_reaction = NaN;
        else
            time_reaction = time_end - time_start;
        end
        trialsummary.(subject){f}(trial,4) = time_reaction;
    end
    
end


%% single subject analysis

ts = trialsummary.(subject);

%% Plot rating

figure;
plotdata.rating(:,1) = cat(1,ts{1}(:,2),ts{2}(:,2));
plotdata.rating(:,2) = cat(1,ts{1}(:,3),ts{2}(:,3));
boxplot([plotdata.rating(plotdata.rating(:,1) == 70,2), plotdata.rating(plotdata.rating(:,1) == 90,2)],'Notch','on')

[~, p_struct.(subject).rating.diff70_90] = ttest(plotdata.rating(plotdata.rating(:,1) == 70,2), plotdata.rating(plotdata.rating(:,1) == 90,2));
[~, p_struct.(subject).rating.diff70_0] = ttest(plotdata.rating(plotdata.rating(:,1) == 70,2));
[~, p_struct.(subject).rating.diff90_0] = ttest(plotdata.rating(plotdata.rating(:,1) == 90,2));


%% Plot reaction times

figure;
plotdata.reaction(:,1) = cat(1,ts{1}(:,2),ts{2}(:,2));
plotdata.reaction(:,2) = cat(1,ts{1}(:,4),ts{2}(:,4));
boxplot([plotdata.reaction(plotdata.reaction(:,1) == 70,2), plotdata.reaction(plotdata.reaction(:,1) == 90,2)],'Notch','on')

[~, p_struct.(subject).reaction.diff70_90] = ttest(plotdata.rating(plotdata.rating(:,1) == 70,2), plotdata.rating(plotdata.rating(:,1) == 90,2));

%% Plot angles

figure;
plotdata.angles(:,1) = ts{3}(:,2);
plotdata.angles(:,2) = ts{3}(:,3);
boxplot([plotdata.angles(plotdata.angles(:,1) == 70,2), plotdata.angles(plotdata.angles(:,1) == 90,2)],'Notch','on')

[~, p_struct.(subject).angles.diff70_90] = ttest(plotdata.angles(plotdata.angles(:,1) == 70,2), plotdata.angles(plotdata.angles(:,1) == 90,2));
[~, p_struct.(subject).angles.diff70_0] = ttest(plotdata.angles(plotdata.angles(:,1) == 70,2));
[~, p_struct.(subject).angles.diff90_0] = ttest(plotdata.angles(plotdata.angles(:,1) == 90,2));



%% Save summaries

if ~exist(fullfile(default.path.clsfres,'Behavior.mat'),'file')
    save(fullfile(default.path.clsfres,'Behavior.mat'),'trialsummary','p_struct');
else
    ss = load(fullfile(default.path.clsfres,'Behavior.mat'));
    ss.trialsummary.(subject) = trialsummary.(subject);
    ss.p_struct.(subject) = p_struct.(subject);
    
    trialsummary = ss.trialsummary;
    p_struct = ss.p_struct;
    save(fullfile(default.path.clsfres,'Behavior.mat'),'trialsummary','p_struct');
    
end

