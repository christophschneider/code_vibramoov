%BEHAVIOR_RUN - Analyzes the subjective reports regarding the experienced
%illusions 
%
% Syntax:  Behavior_Run()
%
% Inputs:
%    none
%
% Outputs:
%    none
%
% Notes: 
%    Select subject folders in the UI, then click 'add' and 'done'.
%
% Other m-files required: Behavior_Settings.m, Behavior_Main.m
% Subfunctions: none
% External files required: sequences files
%
% Author: Christoph Schneider
% Acute Neurorehabilitation Unit (LRNA)
% Division of Neurology, Department of Clinical Neurosciences
% Centre Hospitalier Universitaire Vaudois (CHUV)
% Rue du Bugnon 46, CH-1011 Lausanne, Switzerland
%
% email: christoph.schneider.phd@gmail.com 
% March 2021
%------------- BEGIN CODE --------------

clear;
close all force;
clc;

% Load defaults
% -------------------------------------------------------------------------
Behavior_Settings()

% Select folders
% -------------------------------------------------------------------------
folders = uipickfiles('Prompt','Select Folders','FilterSpec',default.path.behav);

for sub = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(sub),' of ',num2str(size(folders,2)),' ++++']);
    
    % find sub-folders which correspond to sessions
    args.pathname = folders{sub};
    session = dir(fullfile(folders{sub},['*',default.sessionstring,'*']));
    
    % iterate over sessions
    for r = 1:size({session},2)
        
        sesname = {session.name}; sesname = sesname{r};
        % get list of eeg recording files in session folder
        bhvfiles = dir(fullfile(folders{sub},session.name,'behavior','*.log'));
        args.filenames = bhvfiles;
        
        % call analysis function
        Behavior_Main(args);
        
    end
end

disp('---> Batch file processing end <---')