% add dependencies on relative path
pth = fullfile( fileparts(fileparts(mfilename('fullpath'))), '/Final/0 Dependencies' );
addpath(genpath(pth));

% open UI to select folders to process
folders = uipickfiles('Prompt','Select Folders','FilterSpec','/Users/christoph/Desktop/CHUV/Vibramoov');

for sub = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(sub),' of ',num2str(size(folders,2)),' ++++']);
    
    % find files in folder
    args.pathname = folders{sub};
    session = dir(fullfile(folders{sub},['*','ses','*']));
    
    % iterate over sessions
    for ses = 1:size({session.name},2)
        % find recording files
        sesname = {session.name}; sesname = sesname{ses};
        args.files = dir(fullfile(folders{sub},session.name,'eeg','*.gdf'));
        
        % call analysis function
        Conversion(args);
        
    end
end


function [] = Conversion(args)

% automatically set paths via the paths.mat file
% load('channelstructheader.mat');

% iterate across recording files
for f = 1:length(args.files)
    
    eegfilenames = {args.files.name};
    eegfilefolder = {args.files.folder};
    eegfilenameX = eegfilenames{f};
    
    % create file name for updated eeg file
    oldfilename = fullfile(eegfilefolder{f},eegfilenameX);
    [pth,fl,~] = fileparts(oldfilename);
    xt = '.bdf';
    newfilename = fullfile(pth,[fl,xt]);
    eeghdr.filename = newfilename;
    
    hdr = ft_read_header(fullfile(eegfilefolder{f},eegfilenameX));
    data = ft_read_data(fullfile(eegfilefolder{f},eegfilenameX));
    
    eeghdr = readbdfheader(which('template.bdf'));
    
    dt = datetime([num2str(hdr.orig.T0(3)),'.',num2str(hdr.orig.T0(2)),'.',num2str(hdr.orig.T0(1)),'-',num2str(hdr.orig.T0(4)),'.',num2str(hdr.orig.T0(5)),'.',num2str(hdr.orig.T0(6))],'InputFormat','d.M.yyyy-H.m.s');
   
    eeghdr.RecordingDate = char(datestr(dt,'dd.mm.yy'));
    eeghdr.RecordingTime = char(datestr(dt,'HH.MM.SS'));
    eeghdr.headerSize = 256 * hdr.nChans;
    eeghdr.numberOfRecords = hdr.orig.NRec;
    eeghdr.filename = newfilename;
    
%     eeghdr
    
    eegdata = data;

    % update original file in new location
    allocbdffile(eeghdr)
    writebdfdata(eeghdr,eegdata);
    writebdfheader(eeghdr);
    
    
end

end
