% combined classification using gfp and erd features;

close all
clear
clc

%% load data and process to gfp
% load gfp features
load('ERP_new.mat')
avg_gfp = trialavg;


epoch=563;
% figure;hold on;
% for kk=1:length(avg_gfp)       
%     mavg_gfp_illusion(kk,:,:)=squeeze(mean(avg_gfp(kk).illusion.trial,1));    
%     [S_norm, GFPscaling(:,kk)]=renorm_GFP(squeeze(mavg_gfp_illusion(kk,:,:))',[],epoch);
%     plot(GFPscaling(:,kk),'k')
%     time = avg_gfp(kk).illusion.time + 1;
% end

for kk=1:length(avg_gfp)
    for tr = 1:size(avg_gfp(kk).illusion.trial,1)
        mavg_illusion_full(kk,tr,:,:)=squeeze(avg_gfp(kk).illusion.trial(tr,:,:));
        [S_tmp, a] = renorm_GFP(squeeze(mavg_illusion_full(kk,tr,:,:))',[],epoch);
        GFPscaling_full{kk}(tr,1,:) = a;
    end
end


% for kk=1:length(avg)   
%     
%     mavg_ctrl(kk,:,:)=squeeze(mean(avg(kk).control.trial,1));
%     
%     [S_norm, GFPscaling_ctr(:,kk)]=renorm_GFP(squeeze(mavg_ctrl(kk,:,:))',[],epoch);
%     plot(GFPscaling_ctr(:,kk),'r')
% end

for kk=1:length(avg_gfp)
    for tr = 1:size(avg_gfp(kk).control.trial,1)
        mavg_ctrl_full(kk,tr,:,:)=squeeze(avg_gfp(kk).control.trial(tr,:,:));
        [S_tmp, a] = renorm_GFP(squeeze(mavg_ctrl_full(kk,tr,:,:))',[],epoch);
        GFPscaling_ctrl_full{kk}(tr,1,:) = a;
    end
end


% load erd/ers features
load('PSDpreprocresults_final.mat')
avg_erd = avg;


%% classify

V_defaults;
for sub = 1:size(files,2)
    
    % --- gfp features
    
    trials.gfp = cat(1, GFPscaling_full{sub}, GFPscaling_ctrl_full{sub});
    labels.gfp = cat(1, avg_gfp(sub).illusion.trialinfo, avg_gfp(sub).control.trialinfo);
    timevec.gfp = avg_gfp(sub).illusion.time + 1;
    
    % --- erd features
    labels.erd = cat(1, avg_erd(sub).illusion_full.trialinfo, avg_erd(sub).control_full.trialinfo);
    timevec.erd = avg_erd(sub).illusion_full.time + 1;

    tri_bsl = repmat( nanmean(avg_erd(sub).illusion_full.powspctrm(:,:,:,time<=0),4),...
        [1,1,1,length(time)]);
    trc_bsl = repmat( nanmean(avg_erd(sub).control_full.powspctrm(:,:,:,time<=0),4),...
        [1,1,1,length(time)]);

    tri = ( avg_erd(sub).illusion_full.powspctrm -  tri_bsl )./...
        tri_bsl;
    trc = ( avg_erd(sub).control_full.powspctrm -  trc_bsl )./...
        trc_bsl;
    trials.erd = cat(1, tri, trc);
     
    parfor rep = 1:default.xval.reps
        
        disp(['[',num2str(rep),', ',num2str(sub),']'])
        [acc{rep,sub}, cvp{rep,sub}, feats{rep,sub}] = Xval(default, timevec, trials, labels, 'combi');
    end
end

for rep = 1:default.xval.reps
    for sub = 1:size(files,2)
        
        accuracy.train(rep, sub, :) = acc{rep, sub}.train;
        accuracy.test(rep, sub, :) = acc{rep, sub}.test;
        accuracy.testmean(rep, sub, :) = acc{rep, sub}.test_mean;
        features(rep,sub,:) = sum(feats{rep, sub},1)./sum(sum(feats{rep, sub}));
        
    end
end

mean(mean(accuracy.train,3),1)
mean(mean(accuracy.test,3),1)
mean(mean(accuracy.testmean,3),1)

if default.clsf.random
    save('PSD_clsf_random_100x_combi.mat','accuracy')
else
    save('PSD_clsf_10x_combi.mat','accuracy')
end