% This script contains PSD batch analysis for the Vibramoov preprocessed
% data

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2019
% -------------------------------------------------------------------------

addpath('/Users/christoph/Documents/Git/__from_MATLAB_Toolboxes');

clear;
clc;
close all;

args.logging = false;

args.path.source = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_Preprocessed';
args.path.psd = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_PSD';
args.path.save = '/Users/christoph/Desktop/CHUV/Vibramoov/Results';
args.spatialfilter = 'yes';

% not sure if needed
args.spectralfilter = 'yes';
args.filter.high = 40;
args.filter.low = 0.1;
args.baseline = 'yes';
args.baselinetime = [-1.5 -1];


args.loadpsd = 'no';

cd(args.path.source);
files = uipickfiles('Prompt','Select Folders');

args.goodfreqs(1,:) = {'sub-13','sub-14','sub-15','sub-16','sub-17','sub-18','sub-19','sub-20','sub-21','sub-22','sub-23','sub-24','sub-25'};
args.goodfreqs(2,:) = {10,12,10,9,10,7,11,7,10,9,6,9,10};

for sub = 1:size(files,2)
    disp(['++++ Processing file ',num2str(sub),' of ',num2str(size(files,2)),' ++++']);
    args.filename = files{sub};
    args.sub = sub;
    
    % call analysis function
    [h(sub,:),p(sub,:),stat, avg(sub),ratio{sub}] = PSD_body(args);
    %     mask(sub,:,:) = stat.mask;
    %     prob(sub,:,:) = stat.prob;
    %     SCmask(sub,:,:) = stat.singleChannel.mask;
    %     SCprob(sub,:,:) = stat.singleChannel.prob;
end
time = round( avg(1).illusion.time + 1, 2);
freqvec = avg(1).illusion.freq;
% save('clusterresults.mat','mask','prob')

%% Time-Frequency plots

bsltime = time <= 0;

clear A R
for k = 1:numel(avg)
    A = avg(k).illusion.powspctrm;
    R = repmat( nanmean(avg(k).illusion.powspctrm(:,:,bsltime),3), [1,1,size(A,3)]);
    MI(k,:,:,:) = (A - R) ./ R * 100;
end

clear A R
for k = 1:numel(avg)
    A = avg(k).control.powspctrm;
    R = repmat( nanmean(avg(k).control.powspctrm(:,:,bsltime),3), [1,1,size(A,3)]);
    MC(k,:,:,:) = (A - R) ./ R * 100;
end

plotpos = [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
elpos = {'Fz', 'FC3', 'FC2', 'FCz', 'FC2', 'FC4', 'C3', 'C1', 'Cz', 'C2' ,'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};
z1 = min( min(min(min(squeeze(nanmean(MI,1))))), min(min(min(squeeze(nanmean(MC,1))))) );
z2 = max( max(max(max(squeeze(nanmean(MI,1))))), max(max(max(squeeze(nanmean(MC,1))))) );
z = max(abs(z1), abs(z2));

% for sub = 1:numel(avg)

    figure;
    for ch =  1:16
        subplot(4,5,plotpos(ch))
        s = pcolor(squeeze(nanmean(nanmean(MI(:,ch,:,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none');
        caxis([-z, z]) %caxis([z1, z2]) %
        set(gca,'ytick',10:10:length(freqvec),'yticklabels',round(freqvec(10:10:length(freqvec))));
        set(gca,'xtick',11:20:length(time),'xticklabels',time(11:20:length(time)));
        vline(find(time==0),'k');
        vline(find(time==1),'k:');
        vline(find(time==2),'k:');
        vline(find(time==3),'k');
        hold on;
        colormap(redblue)
        title(elpos(ch))
        if ch == 1
            xlabel('Time [s]')
            ylabel('Frequency [Hz]')
        else
            xticklabels({})
            yticklabels({})
        end
        
        % cluster permutation
        [ signif,fpos,nsignif ] = permstattest(squeeze(MI(:,ch,:,:)),1000,0.05,0.05,'ttest',0,3);
        contour(signif,1,'k','LineWidth',3)
    end
    subplot(4,5,1)
ax = axes;
caxis([-z, z]) 
c = colorbar(ax);
c.Ruler.TickLabelFormat='%g%%';
ax.Visible = 'off';
    
% end


% for sub = 1:numel(avg)
    
    figure;
    for ch =  1:16
        subplot(4,5,plotpos(ch))
        s = pcolor(squeeze(nanmean(nanmean(MC(:,ch,:,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none');
        caxis([-z, z]) %caxis([z1, z2]) %
        set(gca,'ytick',10:10:length(freqvec),'yticklabels',round(freqvec(10:10:length(freqvec))));
        set(gca,'xtick',11:20:length(time),'xticklabels',time(11:20:length(time)));
        vline(find(time==0),'k');
        vline(find(time==1),'k:');
        vline(find(time==2),'k:');
        vline(find(time==3),'k');
        hold on;
        colormap(redblue)
        title(elpos(ch))
        if ch == 1
            xlabel('Time [s]')
            ylabel('Frequency [Hz]')
        else
            xticklabels({})
            yticklabels({})
        end


        % cluster permutation
        [ signif,fpos,nsignif ] = permstattest(squeeze(MC(:,ch,:,:)),1000,0.05,0.05,'ttest',0,3);
        contour(signif,1,'k','LineWidth',3)
    end
subplot(4,5,1)
ax = axes;
caxis([-z, z]) 
c = colorbar(ax);
c.Ruler.TickLabelFormat='%g%%';
ax.Visible = 'off';


    
% end

MR = (MI - MC);
z1 = min(min(min(squeeze(nanmean(MR,1)))));
z2 = max(max(max(squeeze(nanmean(MR,1)))));
z = max(abs(z1), abs(z2));

% for sub = 1:numel(avg)
    
    figure;
    for ch =  1:16
        subplot(4,5,plotpos(ch))
        s = pcolor(squeeze(nanmean(nanmean(MR(:,ch,:,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none');
        caxis([-z, z]) %caxis([z1, z2]) %
        set(gca,'ytick',10:10:length(freqvec),'yticklabels',round(freqvec(10:10:length(freqvec))));
        set(gca,'xtick',11:20:length(time),'xticklabels',time(11:20:length(time)));
        vline(find(time==0),'k');
        vline(find(time==1),'k:');
        vline(find(time==2),'k:');
        vline(find(time==3),'k');
        hold on;
        colormap(redblue)
        title(elpos(ch))
        if ch == 1
            xlabel('Time [s]')
            ylabel('Frequency [Hz]')
        else
            xticklabels({})
            yticklabels({})
        end
        
        % cluster permutation
        [ signif,fpos,nsignif ] = permstattest(squeeze(MR(:,ch,:,:)),1000,0.05,0.05,'ttest',0,3);
        contour(signif,1,'k','LineWidth',3)
    end
    subplot(4,5,1)
ax = axes;
caxis([-z, z]) 
c = colorbar(ax);
c.Ruler.TickLabelFormat='%g%%';
ax.Visible = 'off';
    
% end

%% test cluster permutation for differences
for ch = 1:16
    disp(['channel',num2str(ch)])
    [ signif,fpos,nsignif(ch) ] = permstattest(squeeze(MR(:,ch,:,:)),1000,0.1,0.05,'ttest',1,3);
end

%% Topoplots

figure;

cfg = [];
cfg.marker       = 'on';
cfg.elec          = 'standard_1020.elc';

tmp = avg(1).illusion;
tmpcfg = keepfields(cfg, {'layout', 'rows', 'columns', 'commentpos', 'scalepos', 'elec', 'grad', 'opto', 'showcallinfo'});
tmp.dimord = 'freq_chan_time';
tmp.freq = 1;
tmp.time = time;

cfg.comment            = 'no';
cfg.layout = ft_prepare_layout(tmpcfg, tmp);
cfg.layout.pos = cfg.layout.pos * 0.8;
cfg.colormap = colormap(redblue);
% cfg.colorbar = 'South';

% --- averages
% subplot(2,6,1)
subplot(1,2,1)
cfg.xlim         = [0   4];
cfg.zlim         = [-6  6];% [-0.06 0.06]*100;
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,8:12,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title('Grand mu band average');
c = colorbar('south');
c.Ruler.TickLabelFormat='%g%%';

% subplot(2,6,7)
subplot(1,2,2)
cfg.xlim         = [0   4];
cfg.zlim         = [-4  4];%[-0.04 0.04]*100;
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,13:30,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title('Grand beta band average');
c = colorbar('south');
c.Ruler.TickLabelFormat='%g%%';


figure;
% --- time points mu
subplot(2,6,2)
cfg.xlim         = [-0.5   0];
cfg.zlim         = [-0.06 0.06];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,8:12,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,3)
cfg.xlim         = [0   1];
cfg.zlim         = [-0.06 0.06];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,8:12,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,4)
cfg.xlim         = [1   2];
cfg.zlim         = [-0.06 0.06];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,8:12,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,5)
cfg.xlim         = [2   3];
cfg.zlim         = [-0.06 0.06];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,8:12,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,6)
cfg.xlim         = [3   4];
cfg.zlim         = [-0.06 0.06];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,8:12,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

% --- time points beta
subplot(2,6,8)
cfg.xlim         = [-0.5   0];
cfg.zlim         = [-0.04 0.04];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,13:30,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,9)
cfg.xlim         = [0   1];
cfg.zlim         = [-0.04 0.04];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,13:30,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,10)
cfg.xlim         = [1   2];
cfg.zlim         = [-0.04 0.04];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,13:30,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,11)
cfg.xlim         = [2   3];
cfg.zlim         = [-0.04 0.04];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,13:30,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

subplot(2,6,12)
cfg.xlim         = [3   4];
cfg.zlim         = [-0.04 0.04];
y(1,:,:) = squeeze(nanmean(nanmean(MR(:,:,13:30,:),3),1));
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title([num2str(cfg.xlim(1)),' - ',num2str(cfg.xlim(2)),' s']);

%% Classify
V_defaults;
for sub = 1:size(files,2)
    
    labels = cat(1, avg(sub).illusion_full.trialinfo, avg(sub).control_full.trialinfo);
    timevec = avg(sub).illusion_full.time + 1;

    tri_bsl = repmat( nanmean(avg(sub).illusion_full.powspctrm(:,:,:,time<=0),4),...
        [1,1,1,length(time)]);
    trc_bsl = repmat( nanmean(avg(sub).control_full.powspctrm(:,:,:,time<=0),4),...
        [1,1,1,length(time)]);

    tri = ( avg(sub).illusion_full.powspctrm -  tri_bsl )./...
        tri_bsl;
    trc = ( avg(sub).control_full.powspctrm -  trc_bsl )./...
        trc_bsl;
    trials = cat(1, tri, trc);
     
    for rep = 1:default.xval.reps
        
        disp(['[',num2str(rep),', ',num2str(sub),']'])
        [acc{rep,sub}, cvp{rep,sub}, feats{rep,sub}] = Xval(default, timevec, trials, labels, 'PSD');
    end
end

for rep = 1:default.xval.reps
    for sub = 1:size(files,2)
        
        accuracy.train(rep, sub, :) = acc{rep, sub}.train;
        accuracy.test(rep, sub, :) = acc{rep, sub}.test;
        accuracy.testmean(rep, sub, :) = acc{rep, sub}.test_mean;
        features(rep,sub,:) = sum(feats{rep, sub},1)./sum(sum(feats{rep, sub}));
        
    end
end

mean(mean(accuracy.train,3),1)
mean(mean(accuracy.test,3),1)
mean(mean(accuracy.testmean,3),1)

if default.clsf.random
    save('PSD_clsf_random_100x_allfreq_mean0-4s_16chan.mat','accuracy')
else
    save('PSD_clsf_10x_allfreq_mean0-4s_16chan.mat','accuracy')
end

%% features;
meanfeats = squeeze(mean(mean(features,1),2));
fx = 1:length(meanfeats);
freqs = mod(fx,24);
freqs(freqs==0) = 24;
chans = floor((fx-1)./24)+1;

for f = 1:24
   for ch = 1:16
      fmat(f,ch) = meanfeats((freqs==f) & (chans==ch)); 
   end
end

figure;
subplot(1,3,1)
imagesc(fmat)
colormap(redblue)
caxis([-1,1]*max(max(fmat)))
xlabel('channels')
xticks(1:17)
ylabel('frequency [Hz]')
set(gca,'YDir','normal')
yticks(4:5:24);
yticklabels({'10','15','20','25','30'});
colorbar

subplot(1,3,2)
plot(mean(fmat,2))
xticks(4:5:24);
xticklabels({'10','15','20','25','30'});
xlabel('frequency [Hz]')
ylabel('relative frequency')

subplot(1,3,3)

cfg = [];
cfg.marker       = 'on';
cfg.elec          = 'standard_1020.elc';

tmp = avg(1).illusion;
tmpcfg = keepfields(cfg, {'layout', 'rows', 'columns', 'commentpos', 'scalepos', 'elec', 'grad', 'opto', 'showcallinfo'});
tmp.dimord = 'freq_chan';
tmp.freq = 1;
tmp.time = 1;

cfg.comment            = 'no';
cfg.layout = ft_prepare_layout(tmpcfg, tmp);
cfg.layout.pos = cfg.layout.pos * 0.8;
cfg.colormap = colormap(redblue);
cfg.colorbar = 'South';

% --- averages
cfg.xlim         = [1,16];
% cfg.zlim         = [-1, 1] * max(mean(fmat,1));
y(1,:) = mean(fmat,1);
tmp.powspctrm = y;
ft_topoplotTFR(cfg, tmp); title('Grand mu band average');

%%
cfg = [];
grandavg.illusion = ft_freqgrandaverage(cfg, avg(:).illusion);
cfg = [];
grandavg.control = ft_freqgrandaverage(cfg, avg(:).control);
cfg = [];
grandavg.ratio = ft_freqgrandaverage(cfg, ratio{:});
grandavg.ratio.time = round(grandavg.ratio.time + 1,2);


% imagesc(nanmean(cat(3,squeeze(grandavg.ratio.powspctrm(4,:,:)),squeeze(grandavg.ratio.powspctrm(4,:,:))),3))
imagesc(squeeze(nanmean(grandavg.ratio.powspctrm,1)))
set(gca,'ytick',1:2:length(grandavg.ratio.freq),'yticklabels',round(grandavg.ratio.freq(1:2:length(grandavg.ratio.freq))));
set(gca,'YDir','normal')
set(gca,'xtick',1:10:length(grandavg.ratio.time),'xticklabels',grandavg.ratio.time(1:10:length(grandavg.ratio.time)));
colorbar;


for sub = 1:13
    figure;
    for ch = 1:16
        subplot(4,4,ch);
        imagesc(squeeze(ratio{sub}.powspctrm(ch,:,:)))
        set(gca,'YDir','normal')
    end
end


cfg = [];
cfg.layout = 'elec1010.lay';
cfg.interactive = 'yes';
cfg.showoutline = 'yes';
cfg.baseline     = [-1.5 -1];
cfg.baseline         = 'yes';
cfg.baselinetype = 'relative';
figure; ft_multiplotTFR(cfg, grandavg.illusion)
figure; ft_multiplotTFR(cfg, grandavg.control)
figure; ft_multiplotTFR(cfg, grandavg.ratio)

%% Median plots

for k = 1:numel(avg)
    M(k,:,:,:) = median(avg(k).illusion_full.powspctrm,1) ./ median(avg(k).control_full.powspctrm,1);
end

% figure; s = pcolor(squeeze(median(median(M,2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none'); caxis([0.98,1.03])
% set(gca,'ytick',5:5:length(grandavg.ratio.freq),'yticklabels',round(grandavg.ratio.freq(5:5:length(grandavg.ratio.freq))));
% set(gca,'xtick',1:10:length(grandavg.ratio.time),'xticklabels',grandavg.ratio.time(1:10:length(grandavg.ratio.time)));
% vline(11,'k','Vibration on');
% vline(31,'k:');
% vline(51,'k:');
% vline(71,'k','Vibration off');
% rectangle('Position',[31,8,20,4], 'EdgeColor', [1, 0, 0], 'LineWidth',2);
% rectangle('Position',[51,13,2,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);
% rectangle('Position',[71,13,2,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);
%
% figure; s = pcolor(squeeze(median(mean(M,2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none'); caxis([0.98,1.03])
% set(gca,'ytick',5:5:length(grandavg.ratio.freq),'yticklabels',round(grandavg.ratio.freq(5:5:length(grandavg.ratio.freq))));
% set(gca,'xtick',1:10:length(grandavg.ratio.time),'xticklabels',grandavg.ratio.time(1:10:length(grandavg.ratio.time)));
% vline(11,'k','Vibration on');
% vline(31,'k:');
% vline(51,'k:');
% vline(71,'k','Vibration off');
% rectangle('Position',[31,8,20,4]);
% rectangle('Position',[51,13,2,6]);
% rectangle('Position',[71,13,2,6]);

figure;
subplot(2,1,1)
data.a = movmean(squeeze(nanmedian(nanmedian(nanmedian(M(:,:,8:12,:),3),2),1)),2);
sd.a = movmean(squeeze(mad(nanmedian(nanmedian(M(:,:,8:12,:),3),2),1,1)),2)%./sqrt(13);
x = grandavg.illusion.time+1;
[hl, hp] = boundedline(x, data.a, sd.a, 'k','alpha');
vline(0,'k');
vline(0,'k','Vibration on');
vline(1,'k:','Stable interval');
vline(2,'k:');
vline(3,'k','Vibration off');
hline(1,'k:')
yl = ylim;
rectangle('Position',[1.25, yl(1),0.5 ,yl(2)-yl(1)],...
    'FaceColor', [0, 0, 0, 0], ...
    'EdgeColor', [0, 0, 0, 1],...
    'LineWidth', 3);
rectangle('Position',[1.25, yl(1),0.5 ,yl(2)-yl(1)],...
    'FaceColor', [0, 0, 0, 0], ...
    'EdgeColor', [1, 0, 0, 1],...
    'LineWidth', 2);
ylabel('PSD ratio');
legend({'mu band'},'Location','northwest')


subplot(2,1,2)
data.b = movmean(squeeze(nanmedian(nanmedian(nanmedian(M(:,:,13:18,:),3),2),1)),2);
sd.b = movmean(squeeze(mad(nanmedian(nanmedian(M(:,:,13:18,:),3),2),1,1)),2)%./sqrt(13);
x = grandavg.illusion.time+1;
[hl, hp] = boundedline( x, data.b, sd.b, 'k','alpha');
vline(0,'k');
vline(0,'k','Vibration on');
vline(1,'k:','Stable interval');
vline(2,'k:');
vline(3,'k','Vibration off');
hline(1,'k:')
yl = ylim;
rectangle('Position',[2.0, yl(1),0.3 ,yl(2)-yl(1)],...
    'FaceColor', [0, 0, 0, 0], ...
    'EdgeColor', [0, 0, 0, 1],...
    'LineWidth', 3);
rectangle('Position',[2.0, yl(1),0.3 ,yl(2)-yl(1)],...
    'FaceColor', [0, 0, 0, 0], ...
    'EdgeColor', [1, 1, 0.5, 1],...
    'LineWidth', 2);
rectangle('Position',[2.9, yl(1),0.3 ,yl(2)-yl(1)],...
    'FaceColor', [0, 0, 0, 0], ...
    'EdgeColor', [0, 0, 0, 1],...
    'LineWidth', 3);
rectangle('Position',[2.9, yl(1),0.3 ,yl(2)-yl(1)],...
    'FaceColor', [0, 0, 0, 0], ...
    'EdgeColor', [1, 1, 0.5, 1],...
    'LineWidth', 2);
xlabel('time [s]');
ylabel('PSD ratio');
legend({'low beta band'},'Location','northwest')

for k = 1:numel(avg)
    MI(k,:,:,:) = nanmean(avg(k).illusion_full.powspctrm,1) ./ nanmean(nanmedian(avg(k).illusion_full.powspctrm(:,:,:,time<=0),4),1);
end

figure; s = pcolor(squeeze(median(median(MI(:,:,7:20,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none');  caxis([0.96,1.04])
set(gca,'ytick',(1:2:length(grandavg.ratio.freq(7:20)))+1,'yticklabels',round(grandavg.ratio.freq(1:2:length(grandavg.ratio.freq(7:20))))+7);
set(gca,'xtick',1:10:length(grandavg.ratio.time),'xticklabels',grandavg.ratio.time(1:10:length(grandavg.ratio.time)));
vline(11,'k');
vline(31,'k:');
vline(51,'k:');
vline(71,'k');
rectangle('Position',[35,2,12,4], 'EdgeColor', [1, 0, 0], 'LineWidth',2);
rectangle('Position',[51,7,4,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);
rectangle('Position',[69,7,4,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);

for k = 1:numel(avg)
    MC(k,:,:,:) = nanmean(avg(k).control_full.powspctrm,1) ./ nanmean(nanmean(avg(k).control_full.powspctrm(:,:,:,time<=0),4),1);
end

figure; s = pcolor(squeeze(median(median(MC(:,:,7:20,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none');  caxis([0.96,1.04])
set(gca,'ytick',(1:2:length(grandavg.ratio.freq(7:20)))+1,'yticklabels',round(grandavg.ratio.freq(1:2:length(grandavg.ratio.freq(7:20))))+7);
set(gca,'xtick',1:10:length(grandavg.ratio.time),'xticklabels',grandavg.ratio.time(1:10:length(grandavg.ratio.time)));
vline(11,'k');
vline(31,'k:');
vline(51,'k:');
vline(71,'k');
rectangle('Position',[35,2,12,4], 'EdgeColor', [1, 0, 0], 'LineWidth',2);
rectangle('Position',[51,7,4,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);
rectangle('Position',[69,7,4,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);


for k = 1:numel(avg)
    M(k,:,:,:) = median(avg(k).illusion_full.powspctrm,1) ./ median(avg(k).control_full.powspctrm,1);
end
N = squeeze(nanmedian(M,2));



for kx = 1:size(N,3)
    for ky = 1:size(N,2)
        if sum(isnan(N(:,ky,kx))) < size(N,1)
            [~, pN(kx,ky)] = ttest(N(:,ky,kx) - 1);
        else
            pN(kx,ky) = NaN;
        end
    end
end

% mcount = 0;
% for m = 1:2:30
%     mcount = mcount+1;
%     ncount = 0;
%     for n = 1:4:80
%         ncount = ncount + 1;
%         L(:,mcount,ncount) = nanmean(nanmean(N(:,m:m+1,n:n+4),2),3);
%     end
% end
%
% for kx = 1:size(L,3)
%     for ky = 1:size(L,2)
%         if sum(isnan(L(:,ky,kx))) < size(L,1)
%             [pL(kx,ky)] = signrank(L(:,ky,kx),1);
%         else
%             pL(kx,ky) = NaN;
%         end
%     end
% end
%
[ signif,fpos,nsignif ] = permstattest(N-1,1000,0.05,0.05,'ttest',1,3);
% [ signif,fpos,nsignif ] = permstattest(L-1,1000,0.05,0.05,'ttest',1,3);

figure; s = pcolor(squeeze(median(median(MX(2,:,:,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none'); %caxis([0.98,1.03])
set(gca,'ytick',5:5:length(grandavg.ratio.freq),'yticklabels',round(grandavg.ratio.freq(5:5:length(grandavg.ratio.freq))));
set(gca,'xtick',1:10:length(grandavg.ratio.time),'xticklabels',grandavg.ratio.time(1:10:length(grandavg.ratio.time)));
vline(11,'k','Vibration on');
vline(31,'k:');
vline(51,'k:');
vline(71,'k','Vibration off');
hold on;
for kx = 1:size(N,3)
    for ky = 1:size(N,2)
        if pN(kx,ky) < .05
            plot(kx,ky,'*','Color','white')
        end
    end
end
rectangle('Position',[35,8,12,4], 'EdgeColor', [1, 0, 0], 'LineWidth',2);
rectangle('Position',[51,13,4,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);
rectangle('Position',[69,13,4,5], 'EdgeColor', [1, 1, 0.5], 'LineWidth',2);
title('PSD ratio (illusion / control)')
xlabel('Time [s]')
ylabel('Frequency [Hz]')
colorbar

% for one electrode
input = MI;
plotpos = [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
elpos = {'Fz', 'FC3', 'FC2', 'FCz', 'FC2', 'FC4', 'C3', 'C1', 'Cz', 'C2' ,'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};
z1 = min( min(min(min(squeeze(nanmean(MI,1))))), min(min(min(squeeze(nanmean(MC,1))))) );
z2 = max( max(max(max(squeeze(nanmean(MI,1))))), max(max(max(squeeze(nanmean(MC,1))))) );
z = max(abs(z1-1), abs(z2-1));
for sub = 1:numel(avg)
    
    figure;
    for ch =  1:16
        subplot(4,5,plotpos(ch))
        s = pcolor(squeeze(nanmean(nanmean(input(:,ch,:,:),2),1))); set(gca,'YDir','normal'); s.FaceColor = 'interp';set(s, 'EdgeColor', 'none'); caxis([1-z, 1+z])
        set(gca,'ytick',5:5:length(freqvec),'yticklabels',round(freqvec(5:5:length(freqvec))));
        set(gca,'xtick',1:10:length(time),'xticklabels',time(1:10:length(time)));
        vline(find(time==0),'k');
        vline(find(time==1),'k:');
        vline(find(time==2),'k:');
        vline(find(time==3),'k');
        hold on;
        title(elpos(ch))
        xlabel('Time [s]')
        ylabel('Frequency [Hz]')
        if ch == 1
            colorbar
        end
        
    end
    
end

%% test effect sizes

% mu band
K = median(median(median(M(:,:,8:12,35:47),2),3),4);
d = (mean(K)-1)/std(K)
% beta band 1
K = median(median(median(M(:,:,13:18,51:55),2),3),4);
d = (mean(K)-1)/std(K)
% beta band 2
K = median(median(median(M(:,:,13:18,69:73),2),3),4);
d = (mean(K)-1)/std(K)
%% Topoplots

for k = 1:13
    M(k,:,:,:) = median(avg(k).illusion_full.powspctrm,1) ./ median(avg(k).control_full.powspctrm,1);
end

figure;

cfg = [];
cfg.marker       = 'on';
cfg.elec          = 'standard_1020.elc';

tmp = avg(1).illusion;
tmp = rmfield(tmp,'powspctrm');
tmp.powspctrm = squeeze(nanmedian(M,1)) - 1;
cfg.zlim = 'maxmin';
% cfg.interplimits       = 'head';
% cfg.interpolation      = 'nearest'


tmpcfg = keepfields(cfg, {'layout', 'rows', 'columns', 'commentpos', 'scalepos', 'elec', 'grad', 'opto', 'showcallinfo'});
cfg.layout = ft_prepare_layout(tmpcfg, tmp);
cfg.layout.pos = cfg.layout.pos * 0.8;

tmp.time = tmp.time + 1;

subplot(1,3,1)
% cfg.xlim = [1 2];
% cfg.ylim         = [8 12];
cfg.xlim = [1.5 1.8];
cfg.ylim         = [8 15];
cfg.style              = 'straight';
ft_topoplotTFR(cfg, tmp);
title('mu-band 1-2 s');
colorbar;

subplot(1,3,2)
cfg.xlim = [2 2.1];
cfg.ylim         = [13 18];
cfg.style              = 'straight';
ft_topoplotTFR(cfg, tmp);
title('beta-band 2-2.1 s');
colorbar;

% subplot(1,3,3)
% cfg.xlim = [3 3.1];
% cfg.ylim         = [13 18];
% cfg.style              = 'straight';
% ft_topoplotTFR(cfg, tmp);
% title('beta-band 3-3.1 s');
% colorbar;

subplot(1,3,3)
cfg.xlim = [3.2 3.4];
cfg.ylim         = [21 29];
cfg.style              = 'straight';
ft_topoplotTFR(cfg, tmp);
title('beta-band 3-3.1 s');
colorbar;

%% calculate statistical significance
usetime = [0, 1];
useband = [8,12];
% usetime = [1, 1.1];
% useband = [13,20];
usetime = [1.9, 2.1];
useband = [13,18];
for sub = 1:size(avg,2)
    postime = avg(sub).illusion.time <= usetime(2) & avg(sub).illusion.time >= usetime(1);
    posband = avg(sub).illusion.freq < useband(2) & avg(sub).illusion.freq > useband(1);
    data.illusion(sub,:) = mean(mean(avg(sub).illusion.powspctrm(:,posband,postime),2),3);
    data.control(sub,:) = mean(mean(avg(sub).control.powspctrm(:,posband,postime),2),3);
end

[hx, px] = ttest(data.illusion,data.control)

for ch = 1:size(data.illusion,2)
    p2(ch) = signrank(data.illusion(:,ch),data.control(:,ch));
end
p2

for sub = 1:size(ratio,2)
    postime = ratio{sub}.time <= usetime(2) & ratio{sub}.time >= usetime(1);
    posband = ratio{sub}.freq < useband(2) & ratio{sub}.freq > useband(1);
    data.ratio(sub,:) = mean(mean(ratio{sub}.powspctrm(:,posband,postime),2),3);
end
[hr, pr] = ttest(data.ratio-1)

for sub = 1:size(avg,2)
    postime = avg(sub).illusion_full.time <= usetime(2) & avg(sub).illusion_full.time >= usetime(1);
    posband = avg(sub).illusion_full.freq < useband(2) & avg(sub).illusion_full.freq > useband(1);
    data.illusion_full(sub,:) = median(median(median(avg(sub).illusion_full.powspctrm(:,:,posband,postime),3),4),1);
    data.control_full(sub,:) = median(median(median(avg(sub).control_full.powspctrm(:,:,posband,postime),3),4),1);
end
[hy, py] = ttest(data.illusion_full,data.control_full)


mediandata.ratio = median(median(median(M(:,:,posband,postime),3),4),2);
[ha, pe] = ttest(mediandata.ratio - 1)

mediandata.ratio_full = median(median(M(:,:,posband,postime),3),4);
[ha, pe] = ttest(mediandata.ratio_full - 1)

A = squeeze(median(median(M,2),1));
A = squeeze(median(M(:,6,:,:),1));
B = imgaussfilt(A,0.8);
figure; pcolor(B);
data1 = squeeze(median(M,2)) - 1;
for m = 1:size(data1,1)
    data1(m,:,:) = imgaussfilt(data1(m,:,:));
end
[ signif,fpos,nsignif ] = permstattest(data1,1000,0.05,0.05,'ttest',0,3);


%% topoplots
cfg = [];
cfg.baseline     = [-1.5 -1];
cfg.baselinetype = 'relative';
cfg.xlim         = [0   1];
cfg.zlim         = [0.6 1];
cfg.ylim         = [8 12];
cfg.marker       = 'on';
% cfg.layout       = 'elec1010.lay';
cfg.elec          = 'standard_1005.elc';
figure;
subplot(231);ft_topoplotTFR(cfg, grandavg.illusion); title('illusion alpha');
subplot(234);ft_topoplotTFR(cfg, grandavg.control); title('control alpha');
cfg.ylim         = [14 18];
cfg.zlim         = [0.8 1];
subplot(232);ft_topoplotTFR(cfg, grandavg.illusion); title('illusion high alpha');
subplot(235);ft_topoplotTFR(cfg, grandavg.control); title('control high alpha');
cfg.ylim         = [20 30];
cfg.zlim         = [0.9 1];
subplot(233);ft_topoplotTFR(cfg, grandavg.illusion); title('illusion beta');
subplot(236);ft_topoplotTFR(cfg, grandavg.control); title('control beta');

figure;
cfg = [];
cfg.marker       = 'on';
cfg.elec          = 'standard_1005.elc';
cfg.xlim         = [-1.5   1];
cfg.ylim         = [14 15];
% cfg.zlim = [1,16];
significance = grandavg.illusion;
powspctrm = repmat(sum(h)',1,30,length(grandavg.illusion.time));
% powspctrm = repmat(sum(sum(SCmask,1),3)',1,30,length(grandavg.illusion.time));
significance.powspctrm = powspctrm;
ft_topoplotTFR(cfg, significance); title('number of significant channels');

figure;
cfg.xlim         = [-1.5   1];
cfg.ylim         = [14 15];
% cfg.zlim = [1,16];
significance = grandavg.illusion;
powspctrm = repmat(median(p)',1,30,length(grandavg.illusion.time));
% powspctrm = repmat(median(median(SCprob,1),3)',1,30,length(grandavg.illusion.time));
significance.powspctrm = powspctrm;
ft_topoplotTFR(cfg, significance); title('median p-value');




%################################################


%################################################

%% functions
function [h, p, stat, TFR, ratio] = PSD_body(args)


h = NaN;
p = NaN;
stat = [];

%% load data

fn = split(args.filename,filesep);
subjID = fn{end}(1:6);

if strcmp(args.loadpsd,'yes')
    
    filename = fullfile(args.path.psd,[subjID,'_PSD.mat']);
    load(filename,'TFR');
    
elseif strcmp(args.loadpsd,'no')
    
    % loads it to the variable 'data_preprocessed'
    load(args.filename);
    
    % extend pre-stim interval INTRODUCES ARTIFACTUAL RESULTS!!
    %     for tr = 1:numel(data_preprocessed.trial)
    %         mt = data_preprocessed.time{tr} < -1;
    %         tmp.d(:,1:sum(mt)) = flip(data_preprocessed.trial{tr}(:,mt));
    %         tmp.d(:,sum(mt)+1:numel(mt)+sum(mt)) = data_preprocessed.trial{tr};
    %         tmp.t(1:sum(mt)) = data_preprocessed.time{tr}(mt) + data_preprocessed.time{tr}(1);
    %         tmp.t(sum(mt)+1:numel(mt)+sum(mt)) = data_preprocessed.time{tr};
    %
    %         data_preprocessed.trial{tr} = tmp.d;
    %         data_preprocessed.time{tr} = tmp.t;
    %     end
    
        % extend pre-stim interval INTRODUCES ARTIFACTUAL RESULTS!!
    %     for tr = 1:numel(data_preprocessed.trial)
    %         mt = data_preprocessed.time{tr} < -1;
    %         tmp.d(:,1:sum(mt)) = flip(data_preprocessed.trial{tr}(:,mt));
    %         tmp.d(:,sum(mt)+1:numel(mt)+sum(mt)) = data_preprocessed.trial{tr};
    %         tmp.t(1:sum(mt)) = data_preprocessed.time{tr}(mt) + data_preprocessed.time{tr}(1);
    %         tmp.t(sum(mt)+1:numel(mt)+sum(mt)) = data_preprocessed.time{tr};
    %
    %         data_preprocessed.trial{tr} = tmp.d;
    %         data_preprocessed.time{tr} = tmp.t;
    %     end
    
    if strcmp(args.spectralfilter,'yes')
        
        cfg=[];
        cfg.continuous   = 'yes';
        cfg.channel       = 'eeg';
        cfg.padding       = 10;
        cfg.reref         = 'no';
        cfg.lpfilter      = 'yes';
        cfg.lpfreq        = args.filter.high;
        cfg.lpfiltord     = 4;
        cfg.lpfilttype    = 'but';
        cfg.lpfiltdir     = 'twopass';
        
        %         cfg.bpfilter      = 'yes';
        %         cfg.bpfreq        = [args.filter.low args.filter.high];
        %         cfg.bpfiltord     = 4;
        %         cfg.bpfilttype    = 'but';
        %         cfg.bpfiltdir     = 'twopass';
        
        data_preprocessed = ft_preprocessing(cfg,data_preprocessed);
    end
    
    %% baseline voltages
    
    if strcmp(args.baseline,'yes')
        cfg = [];
        cfg.baseline = args.baselinetime;
        cfg.channel = 'eeg';
        data_preprocessed = ft_timelockbaseline(cfg, data_preprocessed);
    end
    
    %% spatial filtering
    
    if strcmp(args.spatialfilter,'yes')
        cfg = [];
        cfg.method      = 'spline';
        cfg.elec        = 'standard_1005.elc';
        cfg.trials      = 'all';
        cfg.feedback    = 'no';
        data_preprocessed    = ft_scalpcurrentdensity(cfg, data_preprocessed);
    end
    
    %% PSD
    
    cfg             = [];
    cfg.output      = 'pow';
    cfg.channel     = 'all';
    cfg.taper       = 'hamming';
    cfg.method      = 'wavelet';
    cfg.width       = 7;
    cfg.pad         = 'nextpow2';
    cfg.foi         = 1:1:30;                         % analysis 2 to 30 Hz in steps of 2 Hz
    cfg.t_ftimwin   = ones(length(cfg.foi),1).*1;   % length of time window = 0.5 sec
    cfg.toi         = -1.5:0.05:3;                  % the time window "slides" from -0.5 to 1.5 in 0.05 sec steps
    
    cfg.trials = find(data_preprocessed.trialinfo==90);
    TFR.illusion = ft_freqanalysis(cfg, data_preprocessed);
    
    cfg.trials = find(data_preprocessed.trialinfo==70);
    TFR.control = ft_freqanalysis(cfg, data_preprocessed);
    
    %---
    
    cfg.keeptrials = 'yes';
    cfg.trials = find(data_preprocessed.trialinfo==90);
    TFR.illusion_full = ft_freqanalysis(cfg, data_preprocessed);
    
    cfg.trials = find(data_preprocessed.trialinfo==70);
    TFR.control_full = ft_freqanalysis(cfg, data_preprocessed);
    
    
    filename = fullfile(args.path.psd,[subjID,'_laplacian_PSD.mat']);
    save(filename,'TFR');
    
end

if args.logging
    TFR.illusion.powspctrm = log(TFR.illusion.powspctrm);
    TFR.control.powspctrm = log(TFR.control.powspctrm);
    TFR.illusion_full.powspctrm = log(TFR.illusion_full.powspctrm);
    TFR.control_full.powspctrm = log(TFR.control_full.powspctrm);
end

% cfg = [];
% cfg.baseline     = [-1.5 -1];
% cfg.baseline         = 'no';
% cfg.baselinetype = 'relative';
% cfg.showlabels   = 'yes';
% cfg.layout = 'elec1010.lay';
% figure; ft_multiplotTFR(cfg, TFR.illusion);
% figure; ft_multiplotTFR(cfg, TFR.control);
% 
% 
cfg = [];
cfg.operation = 'divide';
cfg.parameter = 'powspctrm';
cfg.layout = 'elec1010.lay';
ratio = ft_math(cfg, TFR.illusion, TFR.control);
% figure; ft_multiplotTFR(cfg, ratio);

%% frequency plots

% usetime = TFR.illusion.time > 0 & TFR.illusion.time<1;
% figure; hold on
% plot(TFR.illusion.freq, log(squeeze(nanmean(TFR.illusion.powspctrm(:,:,usetime),3))),'r');
% plot(TFR.illusion.freq, log(squeeze(nanmean(TFR.control.powspctrm(:,:,usetime),3))),'b');
% 
% figure;
% plot(TFR.illusion.freq, squeeze(nanmean(TFR.illusion.powspctrm(:,:,usetime),3)) - squeeze(nanmean(TFR.control.powspctrm(:,:,usetime),3)) ,'b');
% saveas(gcf,[args.path.save,subjID,'freq.png']);

%% Marzia script
%
% usetime = TFR.illusion_full.time > 0 & TFR.illusion_full.time<1;
% % bsltime = TFR.illusion_full.time < -1;
%
% tfr_time=squeeze(nanmean(cat(1,TFR.illusion_full.powspctrm(:,:,:,usetime),TFR.control_full.powspctrm(:,:,:,usetime)),4));
% tfr=squeeze(nanmean(tfr_time,1));
% for ch = 1:size(tfr,1)
%     [~,mm] = findpeaks(tfr(ch,7:13),'SortStr','descend');
%     if ~isempty(mm)
%         pklocs(ch) = mm(1);
%     else
%         pklocs(ch) = NaN;
%     end
% end
% [~,mmx] = findpeaks(mean(tfr(:,7:13),1),'SortStr','descend');
% peaks=pklocs+6;
%
% % if isempty(mmx)
% %    prompt = 'What is the alpha peak? ';
% %    peak = input(prompt);
% % else
% %     peak = mmx(1)+6;
% % end
%
% peak = args.goodfreqs{2,ismember(args.goodfreqs(1,:),subjID)};
% peaks = peak .* ones(size(peaks));
%
% dtfr_illusion=squeeze(nanmean(TFR.illusion_full.powspctrm(:,:,:,usetime),4));
% dtfr_control=squeeze(nanmean(TFR.control_full.powspctrm(:,:,:,usetime),4));
%
%
%
% for k=1:length(peaks)
%     dtfr_illusion_m=dtfr_illusion(:,:,peaks(k));
%     dtfr_control_m=dtfr_control(:,:,peaks(k));
% end
%
% for i=1:size(dtfr_illusion_m,2)
%     [h(i),p(i)]=ttest2(dtfr_illusion_m(:,i),dtfr_control_m(:,i));
% end
%
% freqvec = 6:13;
% for fr = 1:length(freqvec)
%     for i=1:size(dtfr_illusion_m,2)
%         [H_s(i,fr),P_s(i,fr)]=ttest2(dtfr_illusion(:,i,freqvec(fr)),dtfr_control(:,i,freqvec(fr)));
%     end
% end


%% topoplots
% cfg = [];
% cfg.baseline     = [-1.5 -1];
% cfg.baselinetype = 'relative';
% cfg.xlim         = [0   2];
% % cfg.zlim         = [0 1];
% cfg.ylim         = [9 11];
% cfg.marker       = 'on';
% % cfg.layout       = 'elec1010.lay';
% cfg.elec          = 'standard_1005.elc';
% figure;
% subplot(211);ft_topoplotTFR(cfg, TFR.illusion); title('illusion');
% subplot(212);ft_topoplotTFR(cfg, TFR.control); title('control');
%
% cfg = [];
% cfg.operation = 'subtract';
% cfg.parameter = 'powspctrm';
% difference = ft_math(cfg, TFR.illusion, TFR.control);
%
% cfg = [];
% % cfg.baseline     = [-1.5 -1];
% % cfg.baselinetype = 'relative';
% cfg.xlim         = [0   2];
% % cfg.zlim         = [0 1];
% cfg.ylim         = [9 11];
% cfg.marker       = 'on';
% % cfg.layout       = 'elec1010.lay';
% cfg.elec          = 'standard_1005.elc';
% figure;
% subplot(211);ft_topoplotTFR(cfg, difference); title('difference');



%% build neighbors structure
% cfg = [];
% cfg.channel       = TFR.illusion.label;
% cfg.template      = 'elec1010_neighb.mat';
% cfg.elec          = 'standard_1005.elc';
% cfg.method        = 'distance';
% cfg.neighbourdist = 50;
% 
% 
% cfg.method   = 'triangulation';
% cfg.feedback = 'yes'; % visualizes the neighbors
% 
% nb = ft_prepare_neighbours(cfg);
% 
% 
% 
% cfg = [];
% cfg.latency          = [0 1];
% cfg.frequency        = [6 13]; %[peak peak];;
% cfg.method           = 'montecarlo';
% cfg.statistic        = 'ft_statfun_indepsamplesT';
% cfg.correctm         = 'cluster';
% cfg.clusteralpha     = 0.05;
% cfg.clusterstatistic = 'maxsum';
% cfg.minnbchan        = 0;
% cfg.tail             = 0;
% cfg.clustertail      = 0;
% cfg.alpha            = 0.025;
% cfg.numrandomization = 1000;
% cfg.neighbours       = nb;
% 
% cfg.avgovertime = 'yes';
% 
% design = zeros(1,size(TFR.illusion_full.powspctrm,1) + size(TFR.control_full.powspctrm,1));
% design(1,1:size(TFR.illusion_full.powspctrm,1)) = 1;
% design(1,(size(TFR.illusion_full.powspctrm,1)+1):(size(TFR.illusion_full.powspctrm,1)+...
% size(TFR.control_full.powspctrm,1))) = 2;
% 
% cfg.design           = design;
% cfg.ivar             = 1;
% cfg.spmversion = 'spm12';
% 
% for i=1:size(dtfr_illusion_m,2)
%     cfg.channel          = TFR.illusion_full.label{i};
%     stat = ft_freqstatistics(cfg, TFR.illusion_full, TFR.control_full);
%     prob(i,:) = stat.prob;
%     mask(i,:) = stat.mask;
% end
% 
% cfg.channel          = 'all';
% stat = ft_freqstatistics(cfg, TFR.illusion_full, TFR.control_full);
% figure; imagesc(squeeze(stat.prob));
% set(gca,'ytick',1:length(stat.label),'yticklabels',stat.label);
% set(gca,'xtick',1:length(stat.freq),'xticklabels',round(stat.freq));
% 
% stat.singleChannel.prob = prob;
% stat.singleChannel.mask = mask;

%% plot things

% usechan = {'FCz'};
% chanselector = ismember(TFR.illusion.label,usechan);
% 
% a = squeeze(nanmean(nanmean(TFR.illusion_full.powspctrm(:,chanselector,:,usetime),2),4))';
% b = squeeze(nanmean(nanmean(TFR.control_full.powspctrm(:,chanselector,:,usetime),2),4))';
% figure; plot(a,'r')
% hold on;
% plot(-b,'b')
% 
% freqvec = repmat(TFR.illusion.freq',1,2);
% values = cat(2,nanmean(a,2),nanmean(b,2));
% limits = cat(3,repmat(nanstd(a,[],2)./sqrt(size(a,2)),1,2),repmat(nanstd(b,[],2)./sqrt(size(b,2)),1,2));
% 
% maxlim = max(max(nanmax(abs(values)) + abs(squeeze(limits(:,1,:)))));
% minlim = min(nanmin(values));
% padding = ((stat.freq(end) - stat.freq(1)) / (length(stat.freq) - 1) ) / 2;
% sigfreq = stat.freq(mean(stat.mask(chanselector,:),1)>0); % multiple comparisons cluster based permutation mask
% 
% figure;
% if ~isempty(sigfreq)
% rectangle('Position', [sigfreq(1) - padding,0,sigfreq(end)  + padding - sigfreq(1), maxlim], 'EdgeColor',[.8 .8 .8], 'FaceColor', [.8 .8 .8]);
% hold on;
% end
% boundedline(freqvec,values,limits,'alpha');
% set(gca, 'YScale', 'log')
% 
% 
% sigfreq = find(mean(H_s(chanselector,:),1)) + 5; % single channel t-test mask
% zpos = find(~[0 sigfreq 0]);
% [~, grpidx] = max(diff(zpos));
% y = sigfreq(zpos(grpidx):zpos(grpidx+1)-2);
% 
% args.sub
% figure;
% if sum(sigfreq) > 0
% % rectangle('Position', [unique(sigfreq(1) - padding),minlim,sigfreq(end)  + padding - sigfreq(1), maxlim], 'EdgeColor',[.8 .8 .8], 'FaceColor', [.8 .8 .8]);
% rectangle('Position', [unique(sigfreq(1) - padding),100,sigfreq(end)  + padding - sigfreq(1), 13000], 'EdgeColor',[.8 .8 .8], 'FaceColor', [.8 .8 .8]);
% hold on;
% end
% set(gca, 'YScale', 'log')
% boundedline(freqvec,values,limits,'alpha','linewidth',4);
% % legend({'illusion','control'})
% xlim([1,30]);
% ylim([100 13000])
% xlabel('Frequency [Hz]')
% ylabel('Signal power')
% xticks([0,15,30])
% set(gca,'FontSize',40);
% set(gca,'LineWidth',3);
% 
% 
% chanstr = [];
% for c = 1:length(usechan)
% chanstr = [chanstr,usechan{c}];
% end
% 
% saveas(gcf,[args.path.save,subjID,'freq_comparison_',chanstr,'.png']);


%% Classification

% X = cat(1,nanmean(TFR.illusion_full.powspctrm(:,:,7:13,usetime),4),nanmean(TFR.control_full.powspctrm(:,:,7:13,usetime),4));
% X = reshape(X,size(X,1),[]);
% Y = cat(1,TFR.illusion_full.trialinfo,TFR.control_full.trialinfo);

% SVMmdl = fitcsvm(X,Y,'Standardize',true,'OutlierFraction',0.1,'CVPartition',cvpartition(length(Y),'Leaveout'));
% CVSVMmdl = crossval(SVMmdl);
% L = kfoldLoss(SVMmdl,'mode','individual','lossfun','classiferror');
% clsf_out = mean(L);
% YourVector(randperm(length(YourVector)))
% clsf_out = NaN;

% Lambda = logspace(-6,-0.5,11);
% SVMmdl = fitclinear(X,Y,'ObservationsIn','rows','KFold',10,...
%     'Learner','logistic','Solver','sparsa','Regularization','lasso',...
%     'Lambda',Lambda,'GradientTolerance',1e-8);
% ce = kfoldLoss(SVMmdl)

close all
end