% This script contains spectral filtering, epoching, artifact rejection and concatenation

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2019
% -------------------------------------------------------------------------


close all;
clear;
clc;

% *** Function variables
args.path.data = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_TriggerFixed';
args.path.save = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_Preprocessed';
args.events = [90, 70];
args.trialspan = [-1.5, 2.5];
args.filter.low = 1;
args.filter.high = 40;
args.neighbordistance = 50; % in mm
% ***

cd(args.path.data);
folders = uipickfiles('Prompt','Select Folders');

for sub = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(sub),' of ',num2str(size(folders,2)),' ++++']);
    % find files in folder
    args.pathname = folders{sub};
    session = dir(fullfile(folders{sub},['*','ses','*']));
    for ses = 1:size({session.name},2)
        sesname = {session.name}; sesname = sesname{ses};
        %         args.files = dir(fullfile(folders{sub},sesname,'*.*df'));
        args.files = dir(fullfile(folders{sub},session.name,'eeg','*.*df'));
        
        % call analysis function
        preproc_body(args);
        
    end
end


function [] = preproc_body(args)

artifact = struct;

for f = 1:length(args.files)
    
    args.subject = args.files(f).name([4,5] + strfind(args.files(f).name,'sub-'));
    args.session = args.files(f).name([4,5] + strfind(args.files(f).name,'ses-'));
    args.eegfilename = fullfile(args.files(f).folder,args.files(f).name);
    
    
    %% load raw data for plotting/comparisons
    cfg=[];
    cfg.dataset       = args.eegfilename;
    cfg.channel       = 'eeg';
    data_raw = ft_preprocessing(cfg);
    
    %% build neighbors structure
    cfg = [];
    cfg.channel       = data_raw.label;
    cfg.template      = 'elec1010_neighb.mat';
    cfg.elec          = 'standard_1005.elc';
    cfg.method        = 'distance';
    cfg.neighbourdist = args.neighbordistance;
    
    nb = ft_prepare_neighbours(cfg);
    
    %% check for bad channels
    
    % WARNING! For this to work you have to add the following lines in the
    % ft_rejectvisual file of fieldtrip:
    % | cfg.rejchan = ~chansel;
    % | cfg.rejtrl = ~trlsel;
    
    cfg = [];
    cfg.method = 'summary';
    cfg.keepchannel = 'repair';
    cfg.elec          = 'standard_1005.elc';
    cfg.neighbours  = nb;
    data_goodchans = ft_rejectvisual(cfg, data_raw);
    
    artifact(f).chan = data_goodchans.cfg.rejchan;
    
    %% spectral filtering
    cfg=[];
    cfg.continuous   = 'yes';
    cfg.channel       = 'eeg';
    cfg.padding       = 10;
    cfg.reref         = 'no';
    cfg.bpfilter      = 'yes';
    cfg.bpfreq        = [args.filter.low args.filter.high];
    cfg.bpfiltord     = 4;
    cfg.bpfilttype    = 'but';
    cfg.hpfiltdir     = 'twopass';
    
    data_bpfiltered = ft_preprocessing(cfg,data_goodchans);
    
    
    %% create the trial definition and epoch data
    cfg=[];
    cfg.dataset             = args.eegfilename;
    cfg.trialdef.eventtype  = 'STATUS';
    cfg.trialdef.eventvalue = args.events;
    cfg.trialdef.prestim    = - args.trialspan(1);
    cfg.trialdef.poststim   = args.trialspan(2);
    cfg = ft_definetrial(cfg);
    
    trl = cfg.trl;
    
    cfg=[];
    cfg.trl           = trl;
    cfg.padding       = 1;
    data_epoched = ft_redefinetrial(cfg,data_bpfiltered);
    
    %% manual artifact rejection round 1
    
    % eliminate big and infrequent artifacts that can throw off the ICA
    cfg             = [];
    cfg.method      = 'summary';
    cfg.keepchannel = 'repair';
    cfg.elec        = 'standard_1005.elc';
    cfg.neighbours  = nb;
    cfg.keeptrial   = 'nan';
    data_better_tmp = ft_rejectvisual(cfg, data_epoched);
    
    artifact(f).chan  = artifact(f).chan | data_better_tmp.cfg.rejchan;
    artifact(f).trial = find(data_better_tmp.cfg.rejtrl);
    
    
    cfg             = [];
    cfg.method      = 'trial';
    data_better     = ft_rejectvisual(cfg, data_better_tmp);
    
    artifact(f).trial = unique([artifact(f).trial, find(data_better.cfg.rejtrl)]);
    trialnumvec = setdiff(1:numel(data_epoched.trial),artifact(f).trial);
    
    
    %% independent component analysis (ICA)
    
    cfg=[];
    cfg.method  = 'runica';
    cfg.channel = 'eeg';
    
    datacomp = ft_componentanalysis(cfg, data_better);
    
    cfg = [];
    cfg.layout = 'elec1010.lay'; % specify the layout file that should be used for plotting
    cfg.viewmode = 'component';
    
    ica_rej = ft_icabrowser(cfg, datacomp);
    
    cfg = [];
    cfg.demean = 'no';
    cfg.component = find(ica_rej);
    
    data_ica = ft_rejectcomponent(cfg, datacomp);
    
    artifact(f).ica_comp = data_ica.cfg.component;
    
    
    %% manual artifact rejection
    
    % eliminate trials that are still artifact-ridden
    cfg = [];
    cfg.method = 'summary';
    cfg.keepchannel = 'repair';
    cfg.elec          = 'standard_1005.elc';
    cfg.neighbours  = nb;
    cfg.keeptrial   = 'nan';
    data_clean_tmp = ft_rejectvisual(cfg, data_ica);
    
    artifact(f).chan = artifact(f).chan | data_clean_tmp.cfg.rejchan;
    artifact(f).trial = unique([artifact(f).trial, trialnumvec(data_clean_tmp.cfg.rejtrl)]);
    
    cfg = [];
    cfg.method = 'trial';
    data_clean(f) = ft_rejectvisual(cfg, data_clean_tmp);
    
    artifact(f).trial = unique([artifact(f).trial, trialnumvec(data_clean_tmp.cfg.rejtrl)]);
    
    
end

switch length(args.files)
    
    case 1
        data_concat = data_clean(1);%#ok
    case 2
        cfg = [];
        data_concat = ft_appenddata(cfg, data_clean(1), data_clean(2));%#ok
    case 3
        cfg = [];
        data_concat = ft_appenddata(cfg, data_clean(1), data_clean(2), data_clean(3));%#ok
end

savefilename = fullfile(args.path.save,['sub-',args.subject,'_ses-',args.session,'_task-fps_preprocessed.mat']);
save(savefilename,'data_concat','artifact');

end

