This is the Matlab code for the Vibramoov pilot data written by Christoph Schneider in 2019.

To run the scripts make sure Matlab is installed (equal or newer than version 2017b).

The three files PSD, ERP and GFP are the scripts that run the analysis on 
- Power spectral density features (PSD)
- Event related potentials (ERP)
- Global Field Power (GFP)

All scripts rely on the Fieldtrip Matlab toolbox correctly installed.

The code should be readable and quite self-explanatory. If you have any questions, do not hesitate to reach out to me under christoph_schneider@hotmail.ch.