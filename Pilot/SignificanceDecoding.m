clear p H s

files = uipickfiles('Prompt','Select Folders');

for f = 1:numel(files)
    load(files{f});
    if contains(files{f}, 'random')
        s.rnd = round(mean(accuracy.testmean,3),2);
    else
        s.clsf = round(mean(mean(accuracy.testmean,3),1),2);
    end
end

for kk=1:length(s.clsf)
    
    [p(kk),H(kk),~] = signrank(s.rnd(:,kk),s.clsf(kk),'tail','left','alpha',0.05);

end
