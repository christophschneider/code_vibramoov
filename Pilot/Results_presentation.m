% This script evaluates the classification results

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2020
% -------------------------------------------------------------------------

close all;
clear;

args.path.source = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_Preprocessed';
cd(args.path.source);

files = uipickfiles('Prompt','Select Folders');

for f = 1:size(files,2)
    
    if contains(files{f},'rand')
        load(files{f});
        accmat = mean(accuracy.testmean,3);
        for sub = 1:size(accmat,2)
            pd(sub) = fitdist(accmat(:,sub),'Normal');
        end
    else
        load(files{f});
        optacc = mean(mean(accuracy.testmean,3),1);
        optaccmat = mean(accuracy.testmean,3);
    end
end

for sub = 1:length(optacc)
    
    p(sub) = 1 - cdf(pd(sub),round(optacc(sub),2));
end

p