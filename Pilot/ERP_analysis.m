clear
% close all
clc;

addpath(genpath('/Users/christoph/Documents/Git/__from_MATLAB_Toolboxes'))

%% load erd/ers features
load('PSDpreprocresults_final.mat')
avg_erd = avg;

%% load erp features
cd(pwd)
load('ERP_new.mat')
avg = trialavg;

pool.illusion = [];
pool.control = [];
ch = 5;

for sub = 1:numel(avg)
    time = trialavg(sub).illusion.time +1;
    x = squeeze(trialavg(sub).illusion.trial(:,ch,:));
    x = x./std(x(:));
    pool.illusion = cat(1, pool.illusion, x);
    
    x = squeeze(trialavg(sub).control.trial(:,ch,:));
    x = x./std(x(:));
    pool.control = cat(1, pool.control, x);
end

[s.val, s.ord] = sort(mean(pool.illusion(:,time < 3 & time > 0.5),2));
pl.illusion = pool.illusion(s.ord,:); 
[s.val, s.ord] = sort(mean(pool.control(:,time < 3 & time > 0.5),2));
pl.control = pool.control(s.ord,:); 


figure;
subplot(2,2,1);
lim = [quantile(min(pl.illusion,[],2),0.1),quantile(max(pl.illusion,[],2),0.9)];
h1 = pcolor(time,1:size(pl.illusion,1),pl.illusion);
set(h1, 'EdgeColor', 'none');
caxis(lim);
subplot(2,2,2);
% lim = [quantile(min(pl.illusion,[],2),0.1),quantile(max(pl.illusion,[],2),0.9)];
h2 = pcolor(time,1:size(pl.control,1),pl.control);
set(h2, 'EdgeColor', 'none');
caxis(lim);
subplot(2,2,3)
plot(time,mean(pl.illusion,1))
ylim([-0.4,0.4]);
xlim([-0.5,4])
grid on
subplot(2,2,4)
plot(time,mean(pl.control,1))
ylim([-0.4,0.4]);
xlim([-0.5,4])
grid on

%% Cluster permutation test

for sub = 1:size(avg,2)
    plotx.illusion(sub,:,:) = squeeze(mean(trialavg(sub).illusion.trial,1));
    plotx.control(sub,:,:) = squeeze(mean(trialavg(sub).control.trial,1));
end

data1 = squeeze(mean(plotx.illusion,2));
data2 = squeeze(mean(plotx.control,2));

data1 = movmean(data1,5);
data2 = movmean(data2,5);

[ signif,fpos,nsignif ] = permstattest({data1,data2},1000,0.05,0.05,'ttest',0,3);

clusterpos = [find( diff(signif) == 1)+1, find( diff(signif) == -1)+1];

usetime = clusterpos(1,1):clusterpos(1,2);
a = squeeze(mean(mean(plotx.illusion(:,:,usetime),2),3));
b = squeeze(mean(mean(plotx.control(:,:,usetime),2),3));
d = (mean(a) - mean(b))/sqrt((var(a) + var(b))/2);

d_z = mean(a-b)/std(a-b);

[h,p,intx,t] = ttest(a,b);
n = numel(a);
r = corr(a,b);
d = t.tstat * sqrt(2*(1-r)/n);
      
%% plot with topographic channels

yl = [-12, 12];

plotpos = [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
elpos = {'Fz', 'FC3', 'FC2', 'FCz', 'FC2', 'FC4', 'C3', 'C1', 'Cz', 'C2' ,'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};


figure;
cmap = colormap('lines');
for ch =  1:16
    subplot(5,5,plotpos(ch))
    boundedline(time,squeeze(mean(plotx.illusion(:,ch,:),1)),squeeze(std(plotx.illusion(:,ch,:),[],1))/sqrt(numel(avg)) ,'alpha', 'cmap', cmap(1,:));
    hold on;
    boundedline(time,squeeze(mean(plotx.control(:,ch,:),1)),squeeze(std(plotx.control(:,ch,:),[],1))/sqrt(numel(avg)) ,'alpha', 'cmap', cmap(2,:));
    vline(0,'k');
    vline(1,'k:');
    vline(2,'k:');
    vline(3,'k');
    title(elpos(ch))
    xlim([time(1), time(end)])
    ylim(yl)
    set(gca, 'YDir','reverse')
    if ch == 1
        xlabel('Time [s]')
        ylabel('Amplitude [�V]')
    else
        xticklabels({})
        yticklabels({})
    end
end

% subplot(5,5,5)
% boundedline(time,squeeze(mean(mean(plotx.illusion,2),1)),squeeze(std(mean(plotx.illusion,2),[],1))/sqrt(numel(avg)) ,'alpha', 'cmap', cmap(1,:));
% hold on;
% boundedline(time,squeeze(mean(mean(plotx.control,2),1)),squeeze(std(mean(plotx.control,2),[],1))/sqrt(numel(avg)) ,'alpha', 'cmap', cmap(2,:));
% vline(0,'k');
% vline(1,'k:');
% vline(2,'k:');
% vline(3,'k');
% xlabel('Time [s]')
% ylabel('Amplitude [�V]')
% title('channel mean')
% xlim([time(1), time(end)])
% ylim(yl)
% 
% for cl = 1:size(clusterpos,1)
%     rectangle('Position',[time(clusterpos(cl,1)),yl(1),time(clusterpos(cl,2)) - time(find(signif,1,'first')),yl(2)-yl(1)],...
%           'FaceColor', [0, 0, 0, 0.1], ...
%           'EdgeColor', [0, 0, 0, 0.1]);
% end

subplot(4,5,1)
p_1 = plot(1,1);
hold on;
p_2 = plot(1,1);
plot(1,1,'Color',[1,1,1]);
legend([p_1, p_2],{'illusion','control'})
axis off

subplot(5,5,22:24)
boundedline(time,squeeze(mean(mean(plotx.illusion,2),1)),squeeze(std(mean(plotx.illusion,2),[],1))/sqrt(numel(avg)) ,'alpha', 'cmap', cmap(1,:));
hold on;
boundedline(time,squeeze(mean(mean(plotx.control,2),1)),squeeze(std(mean(plotx.control,2),[],1))/sqrt(numel(avg)) ,'alpha', 'cmap', cmap(2,:));
vline(0,'k');
vline(1,'k:');
vline(2,'k:');
vline(3,'k');
xlabel('Time [s]')
ylabel('Amplitude [�V]')
title('channel mean')
xlim([time(1), time(end)])
ylim(yl)
set(gca, 'YDir','reverse')
for cl = 1:size(clusterpos,1)
    rectangle('Position',[time(clusterpos(cl,1)),yl(1),time(clusterpos(cl,2)) - time(find(signif,1,'first')),yl(2)-yl(1)],...
          'FaceColor', [0, 0, 0, 0.1], ...
          'EdgeColor', [0, 0, 0, 0.1]);
end

%% topoplot differences

E.i = mean( plotx.illusion - repmat(mean(plotx.illusion,2),1,16,1), 1);
E.c = mean( plotx.control - repmat(mean(plotx.control,2),1,16,1), 1);

% E.i = mean( plotx.illusion ./ abs(repmat(mean(plotx.illusion,2),1,16,1)), 1) * 100;
% E.c = mean( plotx.control ./ abs(repmat(mean(plotx.control,2),1,16,1)), 1) * 100;

% E.i = mean(plotx.illusion,1);
% E.c = mean(plotx.control,1);

% figure;
cfg = [];
cfg.marker       = 'on';
cfg.elec          = 'standard_1020.elc';

% cfg.xlim         = [1.8   2.3];
% cfg.xlim         = [2   2.3];
cfg.xlim         = [1 2];
cfg.ylim         = [1 1];
% cfg.zlim = [1,16];
tmp = avg(1).illusion;
tmp.powspctrm = E.c% - E.c;
% tmp.powspctrm = mean(E.i,1);
tmp.time = time;
% tmp.powspctrm = median(median(E.i(:,:,signif),3) - median(E.c(:,:,signif),3),1);

tmpcfg = keepfields(cfg, {'layout', 'rows', 'columns', 'commentpos', 'scalepos', 'elec', 'grad', 'opto', 'showcallinfo'});
cfg.layout = ft_prepare_layout(tmpcfg, tmp);
cfg.layout.pos = cfg.layout.pos * 0.8;

tmp.dimord = 'freq_chan_time';
tmp.freq = [1];
tmp = rmfield(tmp,'trial');
tmp = rmfield(tmp,'sampleinfo');

% figure;
% ft_topoplotTFR(cfg, tmp); title('Voltage difference illusion - control 1.8-2.3 s');

% figure;
% % xlimits = [-0.5:0.5:3.5;0:0.5:4];
% xlimits = [-0.5,0:0.1:0.9;0.2:0.1:1];
% for k = 1:9
% subplot(2,5,k)
% cfg.xlim = xlimits(:,k)';
% % cfg.zlim = [];
% % cfg.zlim = [-4, 4] * 10^(-4);
% % cfg.zlim = [-1.7, 1.7] * 10^(-4);
% cfg.zlim = [-1, 1];
% cfg.style              = 'straight';
% ft_topoplotTFR(cfg, tmp); title([num2str(xlimits(1,k)),' - ',num2str(xlimits(2,k)),' s']);
% colormap(redblue)
% end
% signifwin = [round(time(find(signif,1,'first')),2), round(time(find(signif,1,'last')),2)];
% subplot(2,5,10);
% cfg.xlim = signifwin;
% % cfg = rmfield(cfg,'zlim');
% cfg.style              = 'straight';
% ft_topoplotTFR(cfg, tmp); title([num2str(signifwin(1)),' - ',num2str(signifwin(2)),' s']);
% suptitle('Voltage difference illusion - control');
% colormap(redblue)

figure;
xlimits = [-0.5,0:0.1:0.8;0:0.1:0.9];
for k = 1:10
    subplot(2,5,k)
    cfg.xlim = xlimits(:,k)';
    % cfg.zlim = [];
    % cfg.zlim = [-4, 4] * 10^(-4);
    % cfg.zlim = [-1.7, 1.7] * 10^(-4);
    cfg.zlim = [-1.5, 1.5];
    cfg.style              = 'straight';
    ft_topoplotTFR(cfg, tmp); title([num2str(xlimits(1,k)),' - ',num2str(xlimits(2,k)),' s']);
    colormap(redblue)
    if k == 1
        c = colorbar('south');
        c.Ruler.TickLabelFormat='%g ?V';
    end
end
% signifwin = [round(time(find(signif,1,'first')),2), round(time(find(signif,1,'last')),2)];
% subplot(2,5,10);
% cfg.xlim = signifwin;
% % cfg = rmfield(cfg,'zlim');
% cfg.style              = 'straight';
% ft_topoplotTFR(cfg, tmp); title([num2str(signifwin(1)),' - ',num2str(signifwin(2)),' s']);
suptitle('Voltage distribution around mean: illusion - control');
colormap(redblue)


%% classify

V_defaults;
for sub = 1:numel(avg)
    
    % --- erp features
    
    trials.erp = cat(1, avg(sub).illusion.trial, avg(sub).control.trial);
    labels.erp = cat(1, avg(sub).illusion.trialinfo, avg(sub).control.trialinfo);
    timevec.erp = time;
    
%     % --- gfp features
%     
%     trials.gfp = cat(1, GFPscaling_full{sub}, GFPscaling_ctrl_full{sub});
%     labels.gfp = cat(1, avg_gfp(sub).illusion.trialinfo, avg_gfp(sub).control.trialinfo);
%     timevec.gfp = avg_gfp(sub).illusion.time + 1;
%     
    % --- erd features
    labels.erd = cat(1, avg_erd(sub).illusion_full.trialinfo, avg_erd(sub).control_full.trialinfo);
    timevec.erd = avg_erd(sub).illusion_full.time + 1;

    tri_bsl = repmat( nanmean(avg_erd(sub).illusion_full.powspctrm(:,:,:,timevec.erd<=0),4),...
        [1,1,1,length(timevec.erd)]);
    trc_bsl = repmat( nanmean(avg_erd(sub).control_full.powspctrm(:,:,:,timevec.erd<=0),4),...
        [1,1,1,length(timevec.erd)]);

    tri = ( avg_erd(sub).illusion_full.powspctrm -  tri_bsl )./...
        tri_bsl;
    trc = ( avg_erd(sub).control_full.powspctrm -  trc_bsl )./...
        trc_bsl;
    trials.erd = cat(1, tri, trc);
     
    parfor rep = 1:default.xval.reps
        
        disp(['[',num2str(rep),', ',num2str(sub),']'])
%         [acc{rep,sub}, cvp{rep,sub}, feats{rep,sub}] = Xval(default, timevec, trials, labels, 'combi2');
          [acc{rep,sub}, cvp{rep,sub}, feats{rep,sub}] = Xval(default, timevec, trials, labels, 'ERP');
    end
end

for rep = 1:default.xval.reps
    for sub = 1:numel(avg)
        accuracy.train(rep, sub, :) = acc{rep, sub}.train;
        accuracy.test(rep, sub, :) = acc{rep, sub}.test;
        accuracy.testmean(rep, sub, :) = acc{rep, sub}.test_mean;
        features(rep,sub,:) = sum(feats{rep, sub},1)./sum(sum(feats{rep, sub}));
        
    end
end

mean(mean(accuracy.train,3),1)
mean(mean(accuracy.test,3),1)
mean(mean(accuracy.testmean,3),1)

if default.clsf.random
    save('ERP_clsf_random_100x_500-700.mat','accuracy','features')
else
    save('combi2_clsf_10x_ms.mat','accuracy')
end
