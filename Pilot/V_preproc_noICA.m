% This script contains spectral filtering, epoching, artifact rejection and concatenation

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2019
% -------------------------------------------------------------------------


close all;
clear;
clc;

% *** Function variables
args.path.data = '/Users/christoph/Desktop/Vibramoov/Data_TriggerFixed';
args.path.save = '/Users/christoph/Desktop/Vibramoov/Data_Preprocessed/no_ica';
args.events = [90, 70];
args.trialspan = [-1.5, 3];
args.filter.low = 0.1;
args.filter.high = 40;
args.neighbordistance = 50; % in mm
% ***

cd(args.path.data);
folders = uipickfiles('Prompt','Select Folders');

for sub = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(sub),' of ',num2str(size(folders,2)),' ++++']);
    % find files in folder
    args.pathname = folders{sub};
    session = dir(fullfile(folders{sub},['*','ses','*']));
    for ses = 1:size({session.name},2)
        sesname = {session.name}; sesname = sesname{ses};
        %         args.files = dir(fullfile(folders{sub},sesname,'*.*df'));
        args.files = dir(fullfile(folders{sub},session.name,'eeg','*.*df'));
        
        % call analysis function
        preproc_body(args);
        
    end
end


function [] = preproc_body(args)

artifact = struct;

for f = 1:length(args.files)
    
    args.subject = args.files(f).name([4,5] + strfind(args.files(f).name,'sub-'));
    args.session = args.files(f).name([4,5] + strfind(args.files(f).name,'ses-'));
    args.eegfilename = fullfile(args.files(f).folder,args.files(f).name);
    
    
    %% load raw data for plotting/comparisons
    cfg=[];
    cfg.dataset       = args.eegfilename;
    cfg.channel       = 'eeg';
    
    data_raw = ft_preprocessing(cfg);
    
    carry.endsample(f) = data_raw.sampleinfo(end);
    carry.endtime(f) = data_raw.time{1}(end);
    
    %% build neighbors structure
    cfg = [];
    cfg.channel       = data_raw.label;
    cfg.template      = 'elec1010_neighb.mat';
    cfg.elec          = 'standard_1005.elc';
    cfg.method        = 'distance';
    cfg.neighbourdist = args.neighbordistance;
    
    nb = ft_prepare_neighbours(cfg);
    
    %% spectral filtering
    cfg=[];
    cfg.continuous   = 'yes';
    cfg.channel       = 'eeg';
    cfg.padding       = 10;
    cfg.reref         = 'no';
    cfg.bpfilter      = 'yes';
    cfg.bpfreq        = [args.filter.low args.filter.high];
    cfg.bpfiltord     = 4;
    cfg.bpfilttype    = 'but';
    cfg.hpfiltdir     = 'twopass';
    
    data_bpfiltered = ft_preprocessing(cfg,data_raw);
    
    
    %% create the trial definition and epoch data
    cfg=[];
    cfg.dataset             = args.eegfilename;
    cfg.trialdef.eventtype  = 'STATUS';
    cfg.trialdef.eventvalue = args.events;
    cfg.trialdef.prestim    = - args.trialspan(1);
    cfg.trialdef.poststim   = args.trialspan(2);
    
    cfg = ft_definetrial(cfg);
    
    trl = cfg.trl;
    carry.trialnum(f) = size(trl,1);
    
    cfg=[];
    cfg.trl           = trl;
    cfg.padding       = 1;
    
    data_epoched = ft_redefinetrial(cfg,data_bpfiltered);
    
        %% check for bad channels
    
    % WARNING! For this to work you have to add the following lines in the
    % ft_rejectvisual file of fieldtrip:
    % | cfg.rejchan = ~chansel;
    % | cfg.rejtrl = ~trlsel;
    
    cfg = [];
    cfg.method = 'summary';
    cfg.keepchannel = 'repair';
    cfg.elec          = 'standard_1005.elc';
    cfg.neighbours  = nb;
    cfg.keeptrial   = 'yes';
    data_goodchans{f} = ft_rejectvisual(cfg, data_epoched);
    
    carry.artifactchans(f,:) = data_goodchans{f}.cfg.rejchan;
    
    
end

%% append runs

% change sample info and time because otherwise overlaps -> crashes
for f = 2:length(args.files)
    data_goodchans{f}.sampleinfo = data_goodchans{f}.sampleinfo + sum(carry.endsample(1:f-1));
end

data_concat = ft_appenddata(cfg,data_goodchans{:});

%% manual artifact rejection

% eliminate trials that are still artifact-ridden
cfg = [];
cfg.method = 'summary';
cfg.keepchannel = 'repair';
cfg.elec          = 'standard_1005.elc';
cfg.neighbours  = nb;
cfg.keeptrial   = 'nan';
data_clean_tmp = ft_rejectvisual(cfg, data_concat);

artifact_concat.chan = data_clean_tmp.cfg.rejchan;
artifact_concat.trial = find(data_clean_tmp.cfg.rejtrl);

% trialnumvec = setdiff(1:numel(data_concat.trial),artifact_concat.trial);

cfg = [];
cfg.method = 'trial';
data_clean = ft_rejectvisual(cfg, data_clean_tmp);


% artifact_concat.trial = unique([artifact_concat.trial, trialnumvec(data_clean.cfg.rejtrl)]);

%% build artifact structure per run

for f = 1:length(args.files)
    artifact(f).chan = carry.artifactchans(f,:) | artifact_concat.chan;
    artifact(f).trial = artifact_concat.trial((sum(carry.trialnum(1:f-1)) < artifact_concat.trial)...
        & (artifact_concat.trial < sum(carry.trialnum(1:f))));
end

%% save

data_preprocessed = data_clean;
savefilename = fullfile(args.path.save,['sub-',args.subject,'_ses-',args.session,'_task-fps_preprocessed.mat']);
save(savefilename,'data_preprocessed','artifact');

end

