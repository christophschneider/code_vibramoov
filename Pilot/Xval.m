% This function contains ERP cross validation for classification

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2020
% -------------------------------------------------------------------------
% transfer

function [acc, cvp, feat_out] = Xval(config, time, trials, group, type)

if strcmp(type,'combi')
    
    % --- test the input data
    if length(group.gfp) ~= length(group.erd)
        error('dimensions of feature inputs do not match.')
    else
        group = group.erd;
    end
    
    % --- gfp features
    usetime = time.gfp > 0 & time.gfp < 3.5;
    trials.gfp = trials.gfp(:,:,usetime);
    time.gfp = time.gfp(usetime);
    
    trials.gfp = movmean(trials.gfp,125,3);
    trials.gfp = trials.gfp(:,:,1:50:end);
    time.gfp = time.gfp(1:50:end);
    
    for tr = 1:size(trials.gfp,1)
        
        tmp = [];
        for ch = 1:size(trials.gfp,2)
            tmp = [tmp; squeeze(trials.gfp(tr, ch, :))];
        end
        trials_feat_concat.gfp(tr,:) = tmp;
    end
    
    trials.gfp = trials_feat_concat.gfp;
    
    % --- erd features
    starttime = 0; endtime = 3.5;
    usetime = time.erd > starttime & time.erd < endtime;
    trials.erd = nanmean(trials.erd(:,:,:,(usetime)),4);
    time.erd = nanmean(time.erd(usetime));

    features.number_full = size(trials.erd, 2) * size(trials.erd, 3);
    detectNaN = mean(isnan(squeeze(nanmean(trials.erd,1))),1);
    trials.erd = trials.erd(:,:,~detectNaN);
    
    for tr = 1:size(trials.erd,1)
        
        tmp = [];
        for ch = 1:size(trials.erd,2)
            tmp = [tmp; squeeze(trials.erd(tr, ch, :))];
        end
        trials_feat_concat.erd(tr,:) = tmp;
    end
    
    trials.erd = trials_feat_concat.erd;
    
    
    % --- combine features
    trx = trials;
    trials = cat(2,trx.gfp,trx.erd);
    
elseif strcmp(type,'combi2')
    
    % --- test the input data
    if length(group.erp) ~= length(group.erd)
        error('dimensions of feature inputs do not match.')
    else
        group = group.erd;
    end
    
    % --- erp features
    usetime = time.erp > 0.42 & time.erp < 0.8;
%     trials.erp = trials.erp(:,:,usetime);
    trials.erp = squeeze(mean(trials.erp(:,:,usetime),2));
    
    trials.erp = movmean(trials.erp,50,3);
    trials.erp = trials.erp(:,:,1:10:end);
    time.erp = time.erp(1:10:end);
    
%     for tr = 1:size(trials.erp,1)
%         
%         tmp = [];
%         for ch = 1:size(trials.erp,2)
%             tmp = [tmp; squeeze(trials.erp(tr, ch, :))];
%         end
%         trials_feat_concat.erp(tr,:) = tmp;
%     end
%     
%     trials.erp = trials_feat_concat.erp;
    
    % --- erd features
    starttime = 0.5; endtime = 3.5;
    usetime = time.erd > starttime & time.erd < endtime;
    trials.erd = nanmean(trials.erd(:,:,:,(usetime)),4);
    trials.erd = movmean(trials.erd,3,3);
    trials.erd = trials.erd(:,:,2:3:end);
    time.erd = nanmean(time.erd(usetime));

    features.number_full = size(trials.erd, 2) * size(trials.erd, 3);
    detectNaN = mean(isnan(squeeze(nanmean(trials.erd,1))),1);
    trials.erd = trials.erd(:,:,~detectNaN);
    
    for tr = 1:size(trials.erd,1)
        
        tmp = [];
        for ch = 1:size(trials.erd,2)
            tmp = [tmp; squeeze(trials.erd(tr, ch, :))];
        end
        trials_feat_concat.erd(tr,:) = tmp;
    end
    
    trials.erd = trials_feat_concat.erd;
    
    
    % --- combine features
    trx = trials;
    trials = cat(2,trx.erp,trx.erd);
    
elseif strcmp(type,'ERP')
    %% remove time before onset of stimulation
    
%     usetime = time.erp > 0 & time.erp < 3.5;
    usetime = time.erp > 0.42 & time.erp < 0.8;
%      usetime = time.erp >  0.19 & time.erp < 0.76; %0.77 & time.erp < 1.56;

    time = time.erp(usetime);
    trials = squeeze(mean(trials.erp(:,:,usetime),2));
    
%     trials = mean(trials,2);
%     time = mean(time);
    
    trials = movmean(trials,10,2);
    trials = trials(:,1:10:end);
    time = time(1:10:end);
    
    group = group.erp;
    
%     %     usetime = time > 1.8 & time < 2.3;
%     usetime = time > -1 & time < 2.5;
%     trials = trials(:,:,usetime);
%     time = time(usetime);
%     
% %     trials = median(trials,3);
% %     time = median(time);
% 
%     usetime = time.erp > 2.5 & time.erp < 3.5;
%     time = time.erp(usetime);
%     trx(:,1,:) = squeeze(mean(trials.erp(:,:,usetime),2));
%     trx = movmean(trx,50,3);
%     trials = trx(:,1,1:10:end);
%     time = time(1:10:end);
%     
    %% dimensionality reduction + time smoothing (making data more robust)
    
    features.number_full = size(trials, 2) * size(trials, 3);
    
    if features.number_full > config.xval.numstartfeatures * 2
        
        winsize = round(features.number_full / config.xval.numstartfeatures);
        
        for tr = 1:size(trials, 1)
            for el = 1:size(trials, 2)
                counter = 1;
                while winsize*counter <= size(trials, 3)
                    
                    win = (winsize*(counter - 1) + 1) : (winsize * counter);
                    trials_red(tr, el, counter) = mean(squeeze(trials(tr, el, win)));
                    counter = counter + 1;
                end
                
                trials_red(tr, el, counter) = mean( squeeze( trials( tr, el, (winsize*(counter - 1) + 1) : end ) ) );
            end
        end
        
        counter = 1;
        while winsize*counter <= size(trials, 3)
            
            win = (winsize*(counter - 1) + 1) : (winsize * counter);
            time_red(counter) =  mean(squeeze(time(win)));
            counter = counter + 1;
        end
        
        time_red(counter) =  mean( squeeze( time( (winsize*(counter - 1) + 1) : end ) ) );
        
        trials = trials_red;
        time = time_red;
    end
    
    %% reshape features
    
    for tr = 1:size(trials,1)
        
        tmp = [];
        for ch = 1:size(trials,2)
            tmp = [tmp; squeeze(trials(tr, ch, :))];
        end
        trials_feat_concat(tr,:) = tmp;
    end
    
    trials = trials_feat_concat;
    
elseif strcmp(type,'PSDtime')
    
%     % downsample time
%     trials = trials(:,:,:,1:3:end);
%     time = time(1:3:end);
    
    %% remove time before onset of stimulation
%     usetime = (time >= 1.0 & time <= 1.2) | (time >= 2.0 & time <= 2.2);
%     usetime = (time >= 1.0 & time <= 1.2) | (time >= 1.9 & time <= 2.1);
%     usetime = (time >= 1.9 & time <= 2.1);
%     usetime = (time >= 0.5 & time <= 0.8);

    usetime = (time > -1);

    trials = trials(:,:,:,usetime);
    time = time(usetime);
    
%     trials = median(trials,4);
%     time = median(time);
%     
%     % problem with all nan columns at the end
%     trials = trials(:,:,:,1:(end-2));
%     time = time(1:(end-2));
    
    %% cut out uninteresting frequencies
    trials = mean(trials(:,:,8:12,:),3);
    
    %% focus on interesting channels
%     trials = trials(:,:,:,:);
%     trials = median(trials,2);
%     trials = median(trials(:,7:8),2) - median(trials(:,10:11),2);
    
    %% reshape features
    
    for tr = 1:size(trials,1)
        
        tmp = [];
        for ch = 1:size(trials,2)
            tmp1 = [];
            for freq = 1:size(trials,3)
                tmp1 = [tmp1, squeeze(trials(tr, ch, freq ,:))'];
            end
            tmp = [tmp, tmp1];
        end
        trials_feat_concat(tr,:) = tmp;
    end
    
    trials = trials_feat_concat;
    
elseif strcmp(type,'PSD')
    
    %% remove time before onset of stimulation
    starttime = 0; endtime = 4;
    usetime = time > starttime & time < endtime;
    trials = nanmean(trials(:,:,:,(usetime)),4);
    time = time(time > starttime);
    
    %% dimensionality reduction + time smoothing (making data more robust)
    
    features.number_full = size(trials, 2) * size(trials, 3);
    
    if features.number_full > config.xval.numstartfeatures * 2
        
        winsize = round(features.number_full / config.xval.numstartfeatures);
        
        for tr = 1:size(trials, 1)
            for el = 1:size(trials, 2)
                counter = 1;
                while winsize*counter <= size(trials, 3)
                    
                    win = (winsize*(counter - 1) + 1) : (winsize * counter);
                    trials_red(tr, el, counter) = mean(squeeze(trials(tr, el, win)));
                    counter = counter + 1;
                end
                
                trials_red(tr, el, counter) = mean( squeeze( trials( tr, el, (winsize*(counter - 1) + 1) : end ) ) );
            end
        end
        
        counter = 1;
        while winsize*counter <= size(trials, 3)
            
            win = (winsize*(counter - 1) + 1) : (winsize * counter);
            time_red(counter) =  mean(squeeze(time(win)));
            counter = counter + 1;
        end
        
        time_red(counter) =  mean( squeeze( time( (winsize*(counter - 1) + 1) : end ) ) );
        
        trials = trials_red;
        time = time_red;
    end
    
    detectNaN = mean(isnan(squeeze(nanmean(trials,1))),1);
    trials = trials(:,:,~detectNaN);
%     
%     figure;
%     imagesc(squeeze(nanmean(trials(group==90,:,:),1)) ./ squeeze(nanmean(trials(group==70,:,:),1)))
%     
    %% reshape features
    
    for tr = 1:size(trials,1)
        
        tmp = [];
        for ch = 1:size(trials,2)
            tmp = [tmp; squeeze(trials(tr, ch, :))];
        end
        trials_feat_concat(tr,:) = tmp;
    end
    
    trials = trials_feat_concat;
    
    
end

%% start outer fold
cvp.outer = cvpartition(group,'KFold',config.xval.nouter);

for k_out = 1:cvp.outer.NumTestSets
    
    data.outer.train = trials(cvp.outer.training(k_out),:,:);
    data.outer.test = trials(cvp.outer.test(k_out),:,:);
    labels.outer.train = group(cvp.outer.training(k_out));
    labels.outer.test = group(cvp.outer.test(k_out));
    
    %% shuffle labels if random permutation test is selected
    
    if config.clsf.random
        labels.outer.train = labels.outer.train(randperm(length(labels.outer.train)));
    end
    
    
    %% start inner fold feature selection
    
    if strcmp(config.xval.type, 'leaveoneout')
        cvp.inner(k_out) = cvpartition(labels.outer.train,'LeaveOut');
    else
        cvp.inner(k_out) = cvpartition(labels.outer.train,'KFold',config.xval.ninner);
    end
    
    % Filter feature selection based on t-test
    groups = unique(labels.outer.train);
    dat1 = data.outer.train(labels.outer.train == groups(1),:);
    dat2 = data.outer.train(labels.outer.train == groups(2),:);
    
    for feat = 1:size(data.outer.train,2)
        if sum(isnan(dat1(:,feat))) == numel(dat1(:,feat)) || sum(isnan(dat2(:,feat))) == numel(dat2(:,feat))
            p(feat) = nan;
        else
            p(feat) = ranksum(dat1(:,feat), dat2(:,feat));
        end
    end
    
%     [~,a] = sort(p(1:35));
%     [~,b] = sort(p(36:end));
%     
%     dontuse = ones(1,length(p));
%     dontuse([a(1:3),b(1:10)]) = 0;
    
    plim = 0.05;
    dontuse = (p >= plim) | isnan(p);
    
    while sum(dontuse) == length(dontuse)
        plim = plim + 0.05;
        dontuse = (p >= plim) | isnan(p);
    end
    dontuse = logical(dontuse .* 0);
    
    % Wrapper feature selection
    %     fun = @(XT, yT, Xt, yt) loss( fitcecoc(XT, yT), Xt, yt);
    fun = @(XT, yT, Xt, yt) loss( fitcdiscr(XT, yT, 'DiscrimType', 'linear', 'Prior', 'uniform'), Xt, yt);
    
    %     opts = statset('Display','iter');
    opts = statset();
    [features, history] = sequentialfs(fun, data.outer.train, labels.outer.train,...
        'cv', cvp.inner(k_out), 'keepout', dontuse, 'options',opts);

%     [features, history] = sequentialfs(fun, data.outer.train, labels.outer.train,...
%         'cv', cvp.inner(k_out), 'keepout', dontuse, 'options',opts,'direction','backward');

%     featsregister{k_out} = find(features);
    feat_out(k_out,:) = features;
    
    %% end inner fold, back to outer fold
    
    mdl = fitcdiscr(data.outer.train(:,features), labels.outer.train, 'DiscrimType', 'linear', 'Prior', 'uniform');
    
    acc.train(k_out) = mean(mdl.predict(data.outer.train(:,features)) == labels.outer.train);
    acc.test(k_out) = mean(mdl.predict(data.outer.test(:,features)) == labels.outer.test);
    acc.test_mean(k_out) = mean( [mdl.predict( mean( data.outer.test(labels.outer.test == 90,features) ) ) == 90, ...
        mdl.predict( mean( data.outer.test(labels.outer.test == 70,features) ) ) == 70]);
    
%         figure;
%         x = [1:size(data.outer.train,2); 1:size(data.outer.train,2)]';
%         y = [squeeze(median(data.outer.train(labels.outer.train == 90,:),1)); squeeze(median(data.outer.train(labels.outer.train == 70,:),1))]';
%         e = [squeeze(std(data.outer.train(labels.outer.train == 90,:),1)) ./ sqrt(numel(labels.outer.train == 90));...
%             squeeze(std(data.outer.train(labels.outer.train == 70,:) ./ sqrt(numel(labels.outer.train == 70)),1))]';
%         boundedline(x,y,e,'alpha')
%     
%             figure;
%         x = [1:size(data.outer.test,2); 1:size(data.outer.test,2)]';
%         y = [squeeze(median(data.outer.test(labels.outer.test == 90,:),1)); squeeze(median(data.outer.test(labels.outer.test == 70,:),1))]';
%         e = [squeeze(std(data.outer.test(labels.outer.test == 90,:),1)) ./ sqrt(numel(labels.outer.test == 90));...
%             squeeze(std(data.outer.test(labels.outer.test == 70,:) ./ sqrt(numel(labels.outer.test == 70)),1))]';
%         boundedline(x,y,e,'alpha')
    
    cc.a(k_out) = corr(nanmean(data.outer.test(labels.outer.test == 90,:))',nanmean(data.outer.train(labels.outer.test == 90,:))');
    cc.b(k_out) = corr(nanmean(data.outer.test(labels.outer.test == 70,:))',nanmean(data.outer.train(labels.outer.test == 70,:))');
    
    cc.c(k_out) = corr(nanmean(data.outer.test(labels.outer.test == 90, ~dontuse))',nanmean(data.outer.train(labels.outer.test == 90, ~dontuse))');
    cc.d(k_out) = corr(nanmean(data.outer.test(labels.outer.test == 70, ~dontuse))',nanmean(data.outer.train(labels.outer.test == 70, ~dontuse))');
    
    cc.e(k_out) = corr(nanmean(data.outer.test(labels.outer.test == 90, features))',nanmean(data.outer.train(labels.outer.test == 90, features))');
    cc.f(k_out) = corr(nanmean(data.outer.test(labels.outer.test == 70, features))',nanmean(data.outer.train(labels.outer.test == 70, features))');
    
    % feat = find(history.In(1,:));
    % boxdat = [data.outer.train(labels.outer.train == 90, feat);data.outer.test(labels.outer.test == 90, feat);...
    %          data.outer.train(labels.outer.train == 70, feat);data.outer.test(labels.outer.test == 70, feat)];
    % boxgroup = [1 * ones(size(data.outer.train(labels.outer.train == 90, feat))); ...
    %             2 * ones(size(data.outer.test(labels.outer.test == 90, feat)));...
    %             3 * ones(size(data.outer.train(labels.outer.train == 70, feat))); ...
    %             4 * ones(size(data.outer.test(labels.outer.test == 70, feat)))];
    % figure;
    % boxplot(boxdat, boxgroup, 'Notch','on');
    
end
