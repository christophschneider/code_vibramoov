% File containing all user-defined values and parameters for the data
% processing pipeline in field trip

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2019
% -------------------------------------------------------------------------

default.path.data = '/Users/christoph/Desktop/Vibramoov/Data_TriggerFixed/sub-01/ses-01';
default.path.save = '/Users/christoph/Desktop/Vibramoov/Data_Preprocessed/sub-01/ses-01';

default.datafile = 'sub-01_ses-01_task-fps_run-01_eeg.bdf';

default.trial.time.pre = 2; % trial time in seconds before trigger
default.trial.time.post = 3; % trial time in seconds after trigger

default.events.values = [90 70]; % 82/90 = illusion/+plateau, 62/70 = non-illusion/+plateau

default.xval.type = 'KFold';
default.xval.nouter = 10;
default.xval.ninner = 9;
default.xval.numstartfeatures = 500;
default.xval.reps = 10;
default.clsf.method = 'lda';
default.clsf.random = false;

