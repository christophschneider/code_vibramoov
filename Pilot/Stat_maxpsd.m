usetime = TFR.illusion.time > 0 & TFR.illusion.time<1;

tfr_time=squeeze(nanmean(TFR.illusion.powspctrm(:,:,:,usetime),4));
tfr=squeeze(nanmean(tfr_time,1));
[im,mm]=max(tfr(:,3:9)');
peaks=mm+2;


dtfr_illusion=squeeze(nanmean(TFR.illusion.powspctrm(:,:,:,usetime),4));  
dtfr_control=squeeze(nanmean(TFR.control.powspctrm(:,:,:,usetime),4));



for k=1:length(peaks)
dtfr_illusion_m=dtfr_illusion(:,:,peaks(k));
dtfr_control_m=dtfr_control(:,:,peaks(k));
end

for i=1:size(dtfr_illusion_m,2) 
[h(i),p(i)]=ttest2(dtfr_illusion_m(:,i),dtfr_control_m(:,i));
end