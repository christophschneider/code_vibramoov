close all;
clear;
clc;

% *** Function variables
args.path.data = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_TriggerFixed';
args.path.save = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_Preprocessed/no_ica';
args.path.info = '/Users/christoph/Desktop/CHUV/Vibramoov/Info';
% ***

cd(args.path.data);
folders = uipickfiles('Prompt','Select Folders');

for sub = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(sub),' of ',num2str(size(folders,2)),' ++++']);
    % find files in folder
    args.pathname = folders{sub};
    session = dir(fullfile(folders{sub},['*','ses','*']));
    for ses = 1:size({session.name},2)
        sesname = {session.name}; sesname = sesname{ses};
        args.files = dir(fullfile(folders{sub},session.name,'eeg','*.*df'));
        
        for f = 1:length(args.files)
            
            args.subject = args.files(f).name([4,5] + strfind(args.files(f).name,'sub-'));
            args.session = args.files(f).name([4,5] + strfind(args.files(f).name,'ses-'));
            args.run = args.files(f).name([4,5] + strfind(args.files(f).name,'run-'));
            args.eegfilename = fullfile(args.files(f).folder,args.files(f).name);
            
            % call analysis function
            saveBIDS(args);
            
        end
        
    end
end



function [] = saveBIDS(args)

%% load raw data for extracting information
cfg=[];
cfg.dataset       = args.eegfilename;
cfg.channel       = 'eeg';
data_raw = ft_preprocessing(cfg);

recording_duration = round(data_raw.time{1}(end),1);

cfg = [];
cfg.method    = 'copy';
cfg.datatype  = 'eeg';

% specify the input file name, here we are using the same file for every subject
cfg.dataset   = args.eegfilename;

% specify the output directory
cfg.bidsroot  = '/Users/christoph/Desktop/Vibramoov/Data_BIDS';
cfg.sub       = args.subject;
cfg.ses       = args.session;
cfg.run       = args.run;

% dataset description file
cfg.dataset_description.Name = "Disentangling the percepts of illusory movement and sensory stimulation during tendon vibration in the EEG";
cfg.dataset_description.Authors = {'Christoph Schneider', 'Renaud Marquis', 'Jane Johr', 'Marina Da Silva Lopes', 'Philippe Ryvlin', 'Andrea Serino', 'Marzia De Lucia', 'Karin Diserens'};
cfg.dataset_description.Acknowledgements = 'We want to thank Techno Concept and especially Frederic Albert for sharing their extensive knowledge on the topic and for aiding with the technical setup for the tendon vibration. Further we appreciate the work Sophie Aebischer and Elise Marenghi put into reviewing corresponding literature for their bachelor thesis at the Haute ecole de travail social et de la sante (EESP) Lausanne.';
cfg.dataset_description.HowToAcknowledge = "If you reference this dataset in your publications, please acknowledge its authors and cite the paper specified under 'ReferenceAndLinks'. Also, please include the following message: 'This data was obtained from the Open Neuro database.'";
cfg.dataset_description.License = "CC0";
cfg.dataset_description.DatasetDOI = "To be defined";
cfg.dataset_description.Funding = {'This work was supported by the Fondation CHUV, Lausanne, Switzerland; the Loterie Romande, Lausanne, Switzerland; and Techno Concept, Mane, France.'};
cfg.dataset_description.ReferencesAndLinks = {"Insert citation + paper doi"};

% specify the information for the participants.tsv file
% this is optional, you can also pass other pieces of info
% load participant information;
pp = readtable(fullfile(args.path.data,'participants.tsv'),'FileType','text','Delimiter','\t');
cfg.participants.age = pp.age(strcmp(pp.participant_id,['sub-',cfg.sub]));
cfg.participants.sex = pp.sex(strcmp(pp.participant_id,['sub-',cfg.sub]));
cfg.participants.handedness = pp.handedness(strcmp(pp.participant_id,['sub-',cfg.sub]));
cfg.participants.stimulated_side = pp.stimulated_side(strcmp(pp.participant_id,['sub-',cfg.sub]));

% specify the information for the scans.tsv file
timestamp = datetime([data_raw.hdr.StartDate,'.',data_raw.hdr.StartTime],'InputFormat','dd.MM.yy.HH.mm.ss');
cfg.scans.acq_time = datestr(timestamp, 'yyyy-mm-ddThh:MM:SS'); % according to RFC3339

% specify some general information that will be added to the eeg.json file
cfg.InstitutionName             = 'Centre Hospitalier Universitaire Vaudois (CHUV)';
cfg.InstitutionalDepartmentName = 'Laboratoire de recherche en neuroreeducation aigue (LNRA)';
cfg.InstitutionAddress          = 'Rue du Bugnon 46, 1011 Lausanne, Switzerland';

% equipment information
cfg.Manufacturer = "g.tec";
cfg.ManufacturersModelName = "g.Nautilus";
cfg.CapManufacturer = "g.tec";
cfg.CapManufacturersModelName = "g.GAMMAsys";
cfg.DeviceSerialNumber = 'NB-2015.10.70';


% provide the mnemonic and long description of the task
cfg.TaskName        = 'fps';
cfg.TaskDescription = 'Subjects were receiving tendon co-vibration on biceps and triceps on the left elbow. Depending on chosen frequencies, participants did or did not experience illusory movement.';

% these are EEG specific
cfg.eeg.PowerLineFrequency = 50;
cfg.eeg.EEGReference       = 'right ear';
cfg.eeg.EEGGround = 'FPz';
cfg.eeg.EEGPlacementScheme = "10-10";
u.LowCutoff_Hz_ = 0.01;
u.HighCutoff_Hz_ = 100;
u.Roll_off = "6dB/Octave";
v.CenterFrequency_Hz_ = 50;
v.Roll_off = "6dB/Octave";
filters.Bandpassfilter = u;
filters.NotchFilter = v;
cfg.eeg.HardwareFilters = filters;
cfg.eeg.SoftwareFilters = 'n/a';
cfg.eeg.SubjectArtefactDescription = 'n/a';
cfg.eeg.RecordingDuration = recording_duration;
cfg.eeg.RecordingType = "continuous";
% List of hardware (amplifier) filters applied or ideally  key:value pairs of pre-applied filters and their parameter values
% RECOMMENDED. List of temporal hardware filters applied. Ideally key:value pairs of pre-applied hardware filters and their parameter values: e.g., 
% {"HardwareFilters": {"Highpass RC filter": {"Half amplitude cutoff (Hz)": 0.0159, "Roll-off": "6dB/Octave"}}}. Write n/a if no hardware filters applied.

% populate events file with trigger types and durations
trig.val = [62,70,82,90];
trig.duration = [1,2,1,2];
trig.event = {'sham_start','sham_peak','illusion_start','illusion_peak'};

hdr = ft_read_header(cfg.dataset);
trigger = ft_read_event(cfg.dataset, 'header', hdr);
trigger = rmfield(trigger,'offset');
for t = 1:numel(trigger)
      trigger(t).type = trig.event{ismember(trig.val,trigger(t).value)};
      trigger(t).duration = trig.duration(ismember(trig.val,trigger(t).value)) .* hdr.Fs;
end
cfg.trigger.event = trigger;

% Readme file
load(fullfile(args.path.save,['sub-',cfg.sub,'_ses-',cfg.ses,'_task-fps_preprocessed.mat']),'artifact')
status_descriptor = {'good','bad'};
status_vector = status_descriptor(1+artifact(str2double(cfg.run)).chan)';
% add status for trigger channels (first one bad because irregularities)
status_vector = [status_vector;{'bad','good','good','good'}'];
cfg.xtra.channel_status = status_vector; % attached to channels.tsv file in modified data2bids.m
cfg.channels.low_cutoff = 0.1;
cfg.channels.high_cutoff = 100;
cfg.channels.notch = 50;

% Events
tx = table();
tx.onset = [trigger.sample]';
tx.duration = [trigger.duration]';
tx.event_value = [trigger.value]';
tx.event_name = {trigger.type}';
cfg.events = tx;

% Writing instructions for data2bids
cfg.writejson = 'replace';
cfg.writetsv = 'replace';

data2bids(cfg);

end