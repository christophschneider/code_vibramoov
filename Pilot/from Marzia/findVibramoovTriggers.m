function [triggersfinal_TF]=findVibramoovTriggers(trig1,tonset)

%trig1 are read from the txt file that are used to generate triggers (i.e. Protocol1_6min_gradcorrect.txt)
%tonset is the actual trigger read from the EEG data

% map stimulus timing into 500hz (500 datapoints per second)                                                                                                
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
trig1=trig1.data(:,4);   
trig1mult=repmat(trig1,1,100);
trig1mult=trig1mult';
trig1multf=trig1mult(:);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find triggers 
dtrig1=diff(trig1multf);
triggers=find(dtrig1 > 40);
triggers=triggers+1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find triggers identity
triggersfinal=zeros(1,length(trig1multf));
triggersfinal(triggers)=trig1multf(triggers);
triggersfinal(1)=trig1multf(1); %add first trigger of the sequence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add zeros corresponding to the onset of the all sequence
triggersfinal_TF=[zeros(1,tonset-1) triggersfinal];
