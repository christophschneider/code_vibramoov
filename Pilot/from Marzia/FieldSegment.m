function [dataVout]=FieldSegment(dataV,nchans,TriggersV,baseline,poststim,sr)

ind=find(TriggersV);

for kk=1:length(ind)    
    dataVout.trial{kk}=dataV.trial{1}(1:nchans,ind(kk)-baseline:ind(kk)+poststim-1);    
    step=(poststim/sr+baseline/sr)/(poststim+baseline);
    dataVout.time{kk}=[(-baseline+1)/sr:step:poststim/sr];
end



dataVout.hdr.Fs = sr;
dataVout.hdr.nChans = size(dataVout.trial{1},1);
dataVout.hdr.nSamples = size(dataVout.trial{1},2);
dataVout.hdr.nTrials = size(dataVout.trial,2);
dataVout.hdr.label = dataV.label;
dataVout.label=dataV.label;
dataVout.fsample=dataV.fsample;
dataVout.cfg=dataV.cfg;
[itt,v]=find(TriggersV);
dataVout.sampleinfo=[v-baseline+1; v+poststim]';
dataVout.trialinfo=[v; TriggersV(v)]'; 
