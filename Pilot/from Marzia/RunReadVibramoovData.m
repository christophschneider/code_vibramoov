% Run ReadVibramoovData
% addpath('C:\Users\mdelucia\Documents\MATLAB\bdfmatlab\')

%pathdata='C:\Users\mdelucia\Documents\MATLAB\VibramoovProject\Dati\';
pathdata='/Users/christoph/Desktop/Vibramoov/RawData';
pathtrigger='/Users/christoph/Desktop/VibramoovProject';
pathoutput='/Users/christoph/Desktop/Vibramoov/ProcessedData';

% File{1}.name=['Sujet8_partie1'];
% File{2}.name=['Sujet sain8_partie2'];
% File{3}.name=['Sujet 8_partie3'];
% 
% File{4}.name=['Sujet9_partie1.1'];
% File{5}.name=['Sujet9_partie2'];
% File{6}.name=['Sujet9_partie3'];
% 
% File{7}.name=['Sujet10_partie1'];
% File{8}.name=['Sujet10_partie2'];
% File{9}.name=['Sujet10_partie3'];
% 
% File{10}.name=['Sujet11_partie2'];
% File{11}.name=['Sujet11_partie3'];
% File{12}.name=['Sujet11_partie4'];
% 

File{1}.name=['Sujet13_partie1.1'];
File{2}.name=['Sujet13_partie2'];
File{3}.name=['Sujet13_partie3'];

File{4}.name=['Sujet14_partie1'];
File{5}.name=['Sujet14_partie2'];
File{6}.name=['Sujet14_partie3'];

File{7}.name=['Sujet15_partie1'];
File{8}.name=['Sujet15_partie2'];
File{9}.name=['Sujet15_partie3'];

File{10}.name=['Sujet16_partie1'];
File{11}.name=['Sujet16_partie2'];
File{12}.name=['Sujet16_partie3'];

File{13}.name=['Sujet17_partie1'];
File{14}.name=['Sujet17_partie2'];
File{15}.name=['Sujet17_partie3'];

File{16}.name=['Sujet18_partie1'];
File{17}.name=['Sujet18_partie2'];
File{18}.name=['Sujet18_partie3'];

File{19}.name=['Sujet19_partie1'];
File{20}.name=['Sujet19_partie2'];
File{21}.name=['Sujet19_partie3'];

File{22}.name=['Sujet20_partie1'];
File{23}.name=['Sujet20_partie2'];
File{24}.name=['Sujet20_partie3'];

File{25}.name=['Sujet21_partie1'];
File{26}.name=['Sujet21_partie2'];
File{27}.name=['Sujet21_partie3'];

File{28}.name=['Sujet22_partie1'];
File{29}.name=['Sujet22_partie2'];
File{30}.name=['Sujet22_partie3'];

File{31}.name=['Sujet23_partie1'];
File{32}.name=['Sujet23_partie2'];
File{33}.name=['Sujet23_partie3'];

File{34}.name=['Sujet24_partie1'];
File{35}.name=['Sujet24_partie2'];
File{36}.name=['Sujet24_partie3'];

File{37}.name=['Sujet25_partie1'];
File{38}.name=['Sujet25_partie2'];
File{39}.name=['Sujet25_partie3'];

% subj=[8 9 10 11];
subj=[13 14 15 16 17 18 19 20 21 22 23 24 25];

fileselect(subj(1),:)=[1 2 3];
fileselect(subj(2),:)=[4 5 6];
fileselect(subj(3),:)=[7 8 9];
fileselect(subj(4),:)=[10 11 12];
fileselect(subj(5),:)=[13 14 15];
fileselect(subj(6),:)=[16 17 18];
fileselect(subj(7),:)=[19 20 21];
fileselect(subj(8),:)=[22 23 24];
fileselect(subj(9),:)=[25 26 27];
fileselect(subj(10),:)=[28 29 30];
fileselect(subj(11),:)=[31 32 33];
fileselect(subj(12),:)=[34 35 36];
fileselect(subj(13),:)=[37 38 39];



%%% SELECT SUBJECT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subject2select=25;


%%%%%% Parameters for FieldTrip structure %%%%%%
sr=500;
%poststim=1500; %3000ms
poststim=1750; %3.5 sec
baseline=100; %200ms
nchans=16;
cnt.bandpass=[0.1 40];
cnt.epowin(1)=0.2;
cnt.epowin(2)=3.5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


indsubj=find(subj == subject2select);
display(subj(indsubj))
nblocks=3;

for kk = 2:nblocks

datafilename=File{fileselect(subj(indsubj),kk)}.name; % data file
%datafilename='test_03janvier2019_2';
triggerfilename=['Protocol',num2str(kk),'_6min_gradcorrect.txt']; % original trigger sequence
[DataV,HeaderV,TriggersV]=ReadVibramoovData(pathdata,pathtrigger,triggerfilename,datafilename);
clear triggerfilename datafilename
% 
%CHECK TRIGGERS
% figure
% plot(DataV(17,1:100000))
% hold on
% plot(TriggersV(1:100000),'r')

    
Header=HeaderV;
DataEEG=DataV;
Trigs=TriggersV;
% 
% 
% %%%%%  Create Fieldtrip structure %%%%%%%%%%%%%%
% 
dataV=PrepFieldStr(DataEEG,sr,nchans,Header);
% 
% % 
% % %%%%%% Filtering %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % 
dataVFilt=FieldFilt(dataV,cnt);
% 
cfg = [];
cfg.viewmode = 'vertical';
% cfg.channel = {dataV.label{1:20}}
ft_databrowser(cfg,dataV)

dataV=dataVFilt;
clear dataVFilt

save([pathoutput,'Datafilt_subj',num2str(subject2select),'_block',num2str(kk),'.mat'],'dataV')


% %%%%%% Segment %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

dataVout=FieldSegment(dataV,nchans,Trigs,baseline,poststim,sr)


save([pathoutput,'DataSeg_subj',num2str(subject2select),'_block',num2str(kk),'.mat'],'dataVout')

 
%%%%%%%% Prepare layout %%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.channel         = dataVout.label;
cfg.layout          = 'standard_1020.elc';%'EEG1010';%'EEG1010';%'elec1010';
cfg.feedback        = 'no';%'yes';%'no';
lay                 = ft_prepare_layout(cfg);


%%%% Artefact removal %%%%%%
cnt.artefact            = [3];
for task = cnt.artefact
    cfg                 = [];
    cfg.eeg=[1:length(dataVout.label)];
    cfg.layout          = lay;
    cfg.tasks           = task;
    data_noart                = my_artefactremoval2(cfg,dataVout);
end


save([pathoutput,'DatanoArt_subj',num2str(subject2select),'_block',num2str(kk),'.mat'],'data_noart')



clear Header HeaderV DataEEG DataV Trigs TriggersV


end