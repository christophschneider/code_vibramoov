function [data_filt] = filter_data(data, low, high, sr, type)
%function [data_filt] = filter_data(data, low, high, sr, type)
%
%data: chan x sp
%low: low bound of the bandpass filter
%high: hig bound of the bandpass filter
%sr: sampling rate
%type: different functions to be used 1=eeglab 2=fieldtrip

[nchan,nsp] = size(data);

filtorder               = 3*fix(sr/low);
revfilt                 = [];
usefft                  = 0;

%% eeglab
if type == 1

    eeglab;
    
    EEGinput                     = eeg_emptyset;
    EEGinput.data                = data;
    EEGinput.times               = [1:nsp].*(1/sr);
    EEGinput.nbchans             = nchan;
    EEGinput.srate               = sr;
    EEGinput                     = eeg_checkset(EEGinput);

%     neeg                    = nchan;
%     ieeg                    = 1:neeg;
%     neog                    = 0;
%     ieog                    = [];
%     necg                    = 0;
%     iecg                    = [];
% 
%     [EEG]                   = eegMontage(EEG,neeg,ieeg,neog,ieog,necg,iecg);

    EEGinput.setname             = 'test';
    EEGinput                     = eeg_checkset(EEGinput);

    clear neeg ieeg neog ieog necg iecg;

%     EEG.nbchans              = nchan;
%     EEG.chanlocs            = EEG.chanlocs(chanoi);
    EEGinput.chanlocs            = [];
%     EEG.data                = EEG.data(chanoi,:);                                       
    
    %make a structure for the firfilt toolbox:
%     EEGinput.data = data; %channel x time
%     EEGinput.srate = sr;
    EEGinput.trials = 1;
    EEGinput.event = [];
    EEGinput.event(1).eventtype = 'boundary';
    EEGinput.event(1).eventlatency  = 1;
    EEGinput.event(1).eventduration = 0;
    EEGinput.pnts = nsp;
    
    
    EEGout = pop_eegfiltnew(EEGinput,  low, high, filtorder, revfilt, usefft); 
    
    data_filt = EEGout.data;
    clear EEGinput EEGout;

%fieldtrip
elseif type == 2

    %make a structure for the fieldtrip toolbox
        %% create header structure
%     hdr             	= [];
%     hdr.Fs              = sr;
%     hdr.nChans      	= length(chanID);
%     hdr.label           = cell(nchan,1);
%     hdr.label(1:nchan)  = {''};
%     hdr.nSamples        = nsp;
%     hdr.nSamplesPre     = 0;
%     hdr.nTrials         = 1;
%     hdr.orig            = [];
%     hdr.chantype        = [];
%     hdr.chanunit        = [];

    %% create data structure
%     data_ft             = [];
%     data_ft.hdr         = hdr;
%     data_ft.fsample     = sr;
%     data_ft.sampleinfo  = [1 nsp];
%     data_ft.trial       = {double(data)};
%     data_ft.time        = {double([1:nsp].*(1/sr))};
%     data_ft.label       = hdr.label;
%     data_ft.cfg         = [];
%   
%     cfg                 = [];
%     cfg.bpfiltord       = filtorder;
%     cfg.bpfreq          = [low high];
%     cfg.bpfilter        = 'yes';
%     cfg.dir             = 'twopass';
%     cfg.wintype         = 'hamming';
%     cfg.detrend         = 'yes';
%     data_ft_filt        = ft_preprocessing(cfg,data_ft);
% 
%     data_filt           = data_ft_filt.trial{1};


    filtertype              = 'fir';
    dir                     = 'twopass';
    instabilityfix          = 'no';
    wintype                 = 'hamming';

    [data_filt]                  = ft_preproc_bandpassfilter(data, sr, [low high], filtorder, filtertype, dir, instabilityfix, wintype);

end;

end
