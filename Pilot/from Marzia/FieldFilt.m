function [dataVFilt]=FieldFilt(dataV,cnt)


inputdata=dataV.trial{1};
% 
low                     = cnt.bandpass(1);
high                    = cnt.bandpass(2);
sr                      = dataV.fsample;
type                    = 1;%1=eeglab 2=fieldtrip
% 
[data_filt] = filter_data(inputdata, low, high, sr, type);

dataVFilt.trial{1} = data_filt;
dataVFilt.label=dataV.label;
dataVFilt.fsample=dataV.fsample;
dataVFilt.cfg=dataV.cfg;
