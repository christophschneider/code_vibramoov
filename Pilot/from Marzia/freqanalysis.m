function freq=freqanalysis(dataV)

cfg = [];
cfg.method = 'mtmfft'; %'mtmfft', analyses an entire spectrum for the entire data
                       %length, implements multitaper frequency transformation



cfg.method = 'mtmconvol'; %implements multitaper time-frequency
                          %transformation based on multiplication in the
                          %frequency domain.
cfg.taper = 'hanning';
cfg.output = 'pow';
cfg.foi = 1:2:40;
cfg.t_ftimwin    = ones(length(cfg.foi),1).*0.5;   % length of time window = 0.5 sec
cfg.toi          = -0.1980:0.05:3.0;               
freq=ft_freqanalysis(cfg,dataV);
%%
% cfg = [];
% cfg.layout = 'standard_1020.elc';
% ft_singleplotER(cfg,freq)
cfg = [];
cfg.layout = 'standard_1020.elc';
figure
ft_singleplotER(cfg,freq)

cfg = [];
cfg.baseline     = [-0.198 0]; 
cfg.baselinetype = 'absolute'; 
cfg.zlim         = [0 5];	        
cfg.showlabels   = 'yes';	
cfg.layout       = 'standard_1020.elc';



figure
ft_multiplotTFR(cfg, freq);

% figure
% ft_singleplotTFR(cfg, TFRhann);

%ft_singleplotER(cfg,freq)
%% take base 10log
% freq_log10 = freq;
% freq_log10.powspctrm = log10(freq.powspctrm);
% cfg = [];
% cfg.layout = 'standard_1020.elc';
% ft_multiplotER(cfg,freq_log10)