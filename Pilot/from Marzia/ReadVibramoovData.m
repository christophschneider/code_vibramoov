function [DataV,HeaderV,TriggersV]=ReadVibramoovData(pathdata,pathtrigger,triggerfilename,datafilename)

HeaderV = readbdfheader(fullfile(pathdata,datafilename));
DataV = readbdfdata(HeaderV);
tonsets=find(DataV(17,:) > 0);
% tonset=round(mean(tonsets)); % vecchi triggers

dd=diff(tonsets);

tpos=find(dd > 200); % 200 ok for majority, 300 for patients 18 %not working for the third block of 23

tposition=[tonsets(1) tonsets(tpos+1)];

figure
plot(DataV(17,1:50000))
hold on
plot(tposition,10,'or')


% 
trig1=importdata([pathtrigger,triggerfilename]);  % -> 5 datapoints per second




TriggersV=findVibramoovNEWTriggers(trig1,tposition);




