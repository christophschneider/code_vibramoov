% Extraction of crucial parameters and results from the Vibramoov pilot
% study and saving them in .xsl files for further use by the Ergotherapists

clear;
clc;
close all;

%% Parameter graveyard
gotoFolder = '/Users/christoph/Desktop/ResultsVibramoov';
saveFolder = '/Users/christoph/Desktop/ResultsVibramoov';
psd_term = '*psd*.mat';
ep_term = '*EP*.mat';
trial_term = '*Datano*.mat';
fulllabels = {'Fz','FC3','FC1','FCz','FC2','FC4','C3','C1','Cz','C2','C4','CP3','CP1','CPz','CP2','CP4'};
samplingrate = 500;
offset = 0.2; %s
freqvec = 1:40; % for PSD analysis

%% Start execution

listing_psd = dir(fullfile(gotoFolder,psd_term));
listing_ep = dir(fullfile(gotoFolder,ep_term));
listing_trials = dir(fullfile(gotoFolder,trial_term));

% --- PSDs
PSD_tbl = extract_PSD(gotoFolder, listing_psd);
writetable(PSD_tbl.control,fullfile(saveFolder,'Power_control.csv'));
writetable(PSD_tbl.illusion,fullfile(saveFolder,'Power_illusion.csv'));

% --- ERPs
EP_tbl = extract_EP(gotoFolder, listing_ep);
writetable(EP_tbl.control,fullfile(saveFolder,'TimeSeries_control.csv'));
writetable(EP_tbl.illusion,fullfile(saveFolder,'TimeSeries_illusion.csv'));
writetable(EP_tbl.time,fullfile(saveFolder,'TimeSeries_time.csv'));

% --- full trials
trials_tbl = extract_trials(gotoFolder, listing_trials, fulllabels, samplingrate, offset, freqvec, saveFolder);


figure(1);
x = (1:size(trials_tbl.ep,4))./samplingrate - offset;
plotlist= [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

for ch = 1:length(fulllabels)
    
    subplot(4,5,plotlist(ch))
    errorbounds(:,1,1) = squeeze(nanstd(trials_tbl.ep(:,1,ch,:),[],1)./sqrt(size(trials_tbl.ep,1)));
    errorbounds(:,1,2) = squeeze(nanstd(trials_tbl.ep(:,2,ch,:),[],1)./sqrt(size(trials_tbl.ep,1)));
    sig = [squeeze(nanmean(trials_tbl.ep(:,1,ch,:),1))';squeeze(nanmean(trials_tbl.ep(:,2,ch,:),1))'];
    [hl,~] = boundedline(x,sig,errorbounds,'alpha');
    hl(1).LineWidth = 1; hl(2).LineWidth = 1;
    xlim([-offset,3.5])
    title(fulllabels(ch))
    
    variables = {squeeze(trials_tbl.ep(:,1,ch,:)),squeeze(trials_tbl.ep(:,2,ch,:))};
    [signif,fpos,nsignif] = permstattest(variables,1000,0.05,0.05,'ttest',0,1);
    
    if nsignif > 0
        rect.xlim = find(abs(diff(signif)))-1;
        patch([30 60 60 30], [0 0 max(ylim)*[1 1]], [0.8 0.8 0.8])
    end
    
end

figure(2);
x = 1:length(freqvec);
plotlist= [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

for ch = 1:length(fulllabels)
    
    clear errorbounds
    subplot(4,5,plotlist(ch))
    errorbounds(:,1,1) = squeeze(nanstd(trials_tbl.psd(:,1,ch,:),[],1)./sqrt(size(trials_tbl.psd,1)));
    errorbounds(:,1,2) = squeeze(nanstd(trials_tbl.psd(:,2,ch,:),[],1)./sqrt(size(trials_tbl.psd,1)));
    sig = [squeeze(nanmean(trials_tbl.psd(:,1,ch,:),1))';squeeze(nanmean(trials_tbl.psd(:,2,ch,:),1))'];
    [hl,~] = boundedline(x,sig,errorbounds,'alpha');
    hl(1).LineWidth = 1; hl(2).LineWidth = 1;
    xlim([5,25])
    title(fulllabels(ch))
    
    variables = {squeeze(trials_tbl.psd(:,1,ch,:)),squeeze(trials_tbl.psd(:,2,ch,:))};
    [signif,fpos,nsignif] = permstattest(variables,1000,0.05,0.05,'ttest',0,1);
    
    if nsignif > 0
        rect.xlim = find(abs(diff(signif)))-1;
        patch([30 60 60 30], [0 0 max(ylim)*[1 1]], [0.8 0.8 0.8])
    end
    
end

figure(3);
x = 1:length(freqvec);
plotlist= [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

for ch = 1:length(fulllabels)
    
    clear errorbounds
    subplot(4,5,plotlist(ch))
    
    sig = squeeze(nanmean((trials_tbl.psd(:,1,ch,:) - trials_tbl.psd(:,2,ch,:)) ./ ... 
          (trials_tbl.psd(:,1,ch,:) + trials_tbl.psd(:,2,ch,:)),1 ))';
      
    errorbounds = squeeze(nanstd((trials_tbl.psd(:,1,ch,:) - trials_tbl.psd(:,2,ch,:)) ./ ... 
          (trials_tbl.psd(:,1,ch,:) + trials_tbl.psd(:,2,ch,:)),[],1 ))' ./ sqrt(size(trials_tbl.psd,1));
    
    %errorbounds(:,1,1) = squeeze(nanstd(trials_tbl.psd(:,1,ch,:),[],1)./sqrt(size(trials_tbl.ep,1)));
    %errorbounds(:,1,2) = squeeze(nanstd(trials_tbl.psd(:,2,ch,:),[],1)./sqrt(size(trials_tbl.ep,1)));
    %sig = [squeeze(nanmean(trials_tbl.psd(:,1,ch,:),1))';squeeze(nanmean(trials_tbl.psd(:,2,ch,:),1))'];
    
    [hl,~] = boundedline(x,sig,errorbounds,'alpha');
    hl(1).LineWidth = 1;
    xlim([5,25])
    title(fulllabels(ch))
    
    variables = {squeeze((trials_tbl.psd(:,1,ch,:) - trials_tbl.psd(:,2,ch,:)) ./ (trials_tbl.psd(:,1,ch,:) + trials_tbl.psd(:,2,ch,:))),...
        squeeze((trials_tbl.psd(:,1,ch,:) - trials_tbl.psd(:,2,ch,:)) ./ (trials_tbl.psd(:,1,ch,:) + trials_tbl.psd(:,2,ch,:))) .* 0};
    
    
    [signif,fpos,nsignif] = permstattest(variables,1000,0.05,0.05,'ttest',0,1);
    
    if nsignif > 0
        rect.xlim = find(abs(diff(signif)))-1;
        patch([30 60 60 30], [0 0 max(ylim)*[1 1]], [0.8 0.8 0.8])
    end
    
end

disp('**')

%% functions

function [powerspect] = extract_PSD(gotoFolder, listing)
for f = 1:size(listing,1)
    load(fullfile(gotoFolder,listing(f).name));
    subjID(f) = str2double( regexprep( listing(f).name, {'\D*([\d\.]+\d)[^\d]*', '[^\d\.]*'}, {'$1 ', ' '} ) );
    freqvec = round(freq1.freq);
    
    PSDcontrol(f,:) = squeeze(mean(mean(freq1.powspctrm,1),2));
    PSDillusion(f,:) = squeeze(mean(mean(freq2.powspctrm,1),2));
end

powerspect.control = table(subjID',PSDcontrol,'VariableNames',{'SubjectID' 'PSD'});
powerspect.illusion = table(subjID',PSDillusion,'VariableNames',{'SubjectID' 'PSD'});
powerspect.frequencies = freqvec;
end

function [timecourse] = extract_EP(gotoFolder, listing)
for f = 1:size(listing,1)
    load(fullfile(gotoFolder,listing(f).name));
    subjID(f) = str2double( regexprep( listing(f).name, {'\D*([\d\.]+\d)[^\d]*', '[^\d\.]*'}, {'$1 ', ' '} ) );
    timevec = avg_datareref1.time;
    
    EPcontrol(f,:) = squeeze(mean(avg_datareref1.avg,1));
    EPillusion(f,:) = squeeze(mean(avg_datareref2.avg,1));
end

timecourse.control = table(subjID',EPcontrol,'VariableNames',{'SubjectID' 'EP'});
timecourse.illusion = table(subjID',EPillusion,'VariableNames',{'SubjectID' 'EP'});
timecourse.time = table(timevec,'VariableNames',{'timePointInTrial'});%timevec;
end


function [TR] = extract_trials(gotoFolder, listing, fulllabels, samplingrate, offset, freqvec, saveFolder)

for f = 1:size(listing,1)
    load(fullfile(gotoFolder,listing(f).name));
    subjID(f) = str2double( regexprep( listing(f).name, {'\D*([\d\.]+\d)[^\d]*', '[^\d\.]*'}, {'$1 ', ' '} ) );
    condix = unique(datalap_all.trialinfo(:,2));
    bsl = mean(datalap_all.trial(:,:,1:offset*samplingrate),3);
    datalap_all_bsl.trial = datalap_all.trial - repmat(bsl,1,1,size(datalap_all.trial,3));
    
    % --- EP:
    
    % cond 1:
    cond1.means = squeeze(mean(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(1),:,:),1));
    cond1.stds = squeeze(std(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(1),:,:),[],1));
    cond1.sems = squeeze(std(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(1),:,:),[],1))./sqrt(length(datalap_all.trialinfo(:,2)==condix(1)));
    
    % cond 2:
    cond2.means = squeeze(mean(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(2),:,:),1));
    cond2.stds = squeeze(std(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(2),:,:),[],1));
    cond2.sems = squeeze(std(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(2),:,:),[],1))./sqrt(length(datalap_all.trialinfo(:,2)==condix(2)));
    
    x = (1:length(cond1.means(1,:)))./samplingrate - offset;
    plotlist= [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
    
    chex = ismember(fulllabels,datalap_all.label);
    plotlist = plotlist(chex);
    
    for ch = 1:length(datalap_all.label)
        
        clear errorbounds
        gcf1 = figure(1)
        subplot(4,5,plotlist(ch))
        errorbounds(:,1,1) = cond1.stds(ch,:);
        errorbounds(:,1,2) = cond2.stds(ch,:);
        %        errorbounds(:,1,1) = cond1.sems(ch,:);
        %        errorbounds(:,1,2) = cond2.sems(ch,:);
        [hl,~] = boundedline(x,[cond1.means(ch,:);cond2.means(ch,:)],errorbounds,'alpha');
        hl(1).LineWidth = 1; hl(2).LineWidth = 1;
        xlim([-offset,3.5])
        title(datalap_all.label(ch))
        
        TR.ep(f,1,ismember(fulllabels,datalap_all.label(ch)),:) = cond1.means(ch,:);
        TR.ep(f,2,ismember(fulllabels,datalap_all.label(ch)),:) = cond2.means(ch,:);
        
        % --- PSDs
        
        % cond 1:
        sig = squeeze(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(1),ch,:))';
        [pxx,~] = pwelch(sig,200,150,freqvec,samplingrate);
        TR.psd(f,1,ismember(fulllabels,datalap_all.label(ch)),:) = mean(pxx,2);
        pow.cond1.std(ch,:) = std(pxx,[],2);
        variables{1} = pxx';
        
        % cond 2:
        sig = squeeze(datalap_all_bsl.trial(datalap_all.trialinfo(:,2)==condix(2),ch,:))';
        [pxx,~] = pwelch(sig,200,150,freqvec,samplingrate);
        TR.psd(f,2,ismember(fulllabels,datalap_all.label(ch)),:) = mean(pxx,2);
        pow.cond2.std(ch,:) = std(pxx,[],2);
        variables{2} = pxx';
        
        clear errorbounds
        gcf2 = figure(2)
        subplot(4,5,plotlist(ch))
        errorbounds(:,1,1) = pow.cond1.std(ch,:);
        errorbounds(:,1,2) = pow.cond2.std(ch,:);
        [hl,~] = boundedline(freqvec,[squeeze(log(TR.psd(f,1,ch,:)))';squeeze(log(TR.psd(f,2,ch,:)))'],errorbounds,'alpha');
        hl(1).LineWidth = 1; hl(2).LineWidth = 1;
        title(datalap_all.label(ch))
        xlim([4,40])
        
        [signif,fpos,nsignif] = permstattest(variables,1000,0.05,0.05,'ttest2',0,1);
        if nsignif > 0
            rect.xlim = [find(signif,1,'first'),find(signif,1,'last')];
            patch([rect.xlim(1) rect.xlim(2) rect.xlim(2) rect.xlim(1)], [min(ylim)*[1 1] max(ylim)*[1 1]], [0.8 0.8 0.8],'EdgeColor','none','FaceAlpha',0.5)
        end
    end
    
    
    set(gcf1, 'Position', get(0, 'Screensize'));
    plotname = fullfile(saveFolder,['EP_',num2str(subjID(f)),'.png']);
    saveas(gcf1,plotname)
    set(gcf2, 'Position', get(0, 'Screensize'));
    plotname = fullfile(saveFolder,['PSD_',num2str(subjID(f)),'.png']);
    saveas(gcf2,plotname)
    
    % replace zeros in all-subj matrix wit NaN
    elim = find(squeeze(sum(TR.ep(f,1,:,:),4))==0);
    if ~isempty(elim)
        TR.ep(f,1,elim,:) = NaN(1,size(cond1.means,2));
        TR.ep(f,2,elim,:) = NaN(1,size(cond1.means,2));
    end
    
    % replace zeros in all-subj matrix wit NaN
    elim = find(squeeze(sum(TR.psd(f,1,:,:),4))==0);
    if ~isempty(elim)
        TR.psd(f,1,elim,:) = NaN(1,length(freqvec));
        TR.psd(f,2,elim,:) = NaN(1,length(freqvec));
    end
    
    close 1 2
    
end

TR.subject = subjID;

end
