function [data] = my_Vibramoov2ft(y,sr,chanID,chanName)

% 
% GTEC2FT converts continuous data recorded with GTEC simulink software to 
%         fieldtrip readable input data structure.
% 
% INPUT
%     y           data array of size [channels x 1 x time] or [channels x time]
%                 assumes that first channel contains time information (sec) and starts 
%                 counting channel indices from the second channel (index = 1) onward
%     sr          sampling rate
%     chanID      channel indices (used for selecting and re-ordering channels for the
%                 output data
%     chanName    cell array of channel names, e.g. {'Fz' 'Cz' 'Pz'}
% 
% OUTPUT
%     data        fieldtrip compatible data structure with empty cfg file
%
% Author, date: MDL, 9/11/2018

    'RUNNING MY_Vibramoov2FT FUNCTION'

    %% if necessary, reduce input data dimensions to 2
    if ndims(y)>2
        y = squeeze(y);%chan x time
    end;

    %check for missing data (nan) inconsistencies between datasets
%     ind_eeg  = sum(isnan(data_eeg.trial{1}),1)>0;
%     ind_eogh = sum(isnan(data_eogh.trial{1}),1)>0;
%     ind_eogv = sum(isnan(data_eogv.trial{1}),1)>0;
%     ind_ecg  = sum(isnan(data_ecg.trial{1}),1)>0;
%     ind_all  =

%     ind = sum(isnan(y),1)>0;%1 x time
%     if sum(ind)>0
%         y(:,ind)=NaN;
%     end;

    ind = isnan(y(1,:));%1 x time
    if sum(ind)>0
        y(1,ind)=([find(ind)]-1).*(1/sr);
    end;

    %% extract relevant data
    time = y(1,:);%sec
    chanData = y(chanID+1,:);%chan x time; data of interest, but note that 
                             %because first row is time we have to add 1 to 
                             %channel indices

    %% if necessary, transpose channel name array to n x 1
    [len1,len2] = size(chanName);
    if len2>len1
        chanName = chanName';
    end;

    %% create header structure
    hdr = [];
    hdr.Fs = sr;
    hdr.nChans = length(chanID);
    hdr.label = chanName;
    hdr.nSamples = size(chanData,2);
    hdr.nSamplesPre = 0;
    hdr.nTrials = 1;
    hdr.orig = [];
    hdr.chantype = [];
    hdr.chanunit = [];

    %% create data structure
    data = [];
    data.hdr = hdr;
    data.fsample = sr;
    data.sampleinfo = [1 size(chanData,2)];
    data.trial = {double(chanData)};
    data.time = {double(time)};
    data.label = chanName;
    data.cfg = [];
  
end