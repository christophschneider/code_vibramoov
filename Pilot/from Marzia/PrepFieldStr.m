function dataV=PrepFieldStr(DataV1,sr,nchans,HeaderV1)

%%%%%% Preparing FieldTrip data structure %%%%%%

y(1,:)=[1:size(DataV1,2)]/sr;
y(2:1+nchans,:)=DataV1(1:nchans,:);
chanID=[1:nchans];
% chanName=HeaderV1.Channel(1:end-1).Label;
for i=1:nchans
   chanName{i}=HeaderV1.Channel(i).Label;
end
[dataV] = my_Vibramoov2ft(y,sr,chanID,chanName);


% this step gives the possibility to rereference to another channel
cfg                     = [];
cfg.reref               = 'no'; 
cfg.refchannel          = [];
dataV                = ft_preprocessing(cfg, dataV);
cfg                     = [];
cfg.channel             = chanName;
dataV                = ft_preprocessing(cfg, dataV);

dataV.fsample=sr;
% 