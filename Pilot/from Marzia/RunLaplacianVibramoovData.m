% subj=[8 9 10 11];
%subj=[8 9 10 11];
subj=[13 14 15 16 17 18 19 20 21 22 23 24 25];

subject2select=21;
nblocks=3;
trcode=[42 62];
pathoutput='C:\Users\mdelucia\Documents\MATLAB\VibramoovProject\ProcessedData\';

data_all=[];


for kk=1:nblocks
    
    cfg=[];
    
    load([pathoutput,'DatanoArt_subj',num2str(subject2select),'_block',num2str(kk),'.mat'])
    
    if kk == 1
        data_all=data_noart;
    else
        data_all=ft_appendtimelock(cfg,data_all,data_noart);
    end
end

%%%% Visualize non-artifacted segemented data %%%%%%
cfg = [];
cfg.viewmode = 'vertical';
% cfg.channel = {dataV.label{1:20}}
ft_databrowser(cfg,data_all)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


cfg = [];
cfg.channel         = data_all.label;
cfg.layout          = 'standard_1020.elc';%'EEG1010';%'EEG1010';%'elec1010';
cfg.feedback        = 'no';%'yes';%'no';
lay                 = ft_prepare_layout(cfg);


%%%%
ind                 = ones(size(lay.label));
n                   = length(ind);
for i = 1:n
    if sum(strcmpi(data_all.label,lay.label(i)))==0
        ind(i)      = 0;
    end;
end;
ind                 = logical(ind);
lay.pos             = lay.pos(ind,:);
lay.width           = lay.width(ind,:);
lay.height          = lay.height(ind,:);
lay.label           = lay.label(ind,:);
%%%




cfg=[];
cfg.method        = 'distance';
cfg.neighbourdist = 0.5;
%cfg.template      = name of the template file, e.g. CTF275_neighb.mat
cfg.layout        = lay; %filename of the layout, see FT_PREPARE_LAYOUT
cfg.channel       = data_all.label; %channels for which neighbours should be found
neighbours        = ft_prepare_neighbours(cfg, data_all);


datalap_all       = data_all;

for k=1:length(data_all.label)
    
    ind=ones(1,length(data_all.label));

    for jj=1:length(data_all.label)
        if sum(strcmpi(data_all.label(jj),neighbours(k).neighblabel))
            ind(jj)=0;
        end
    end
    
    ind=logical(ind);
    
    data2average.trial=data_all.trial;
    data2average.trial(:,ind,:)=[];
    
    
    datalap_all.trial(:,k,:)=data_all.trial(:,k,:)-mean(data2average.trial,2);
    
end


%%%% Visualize non-artifacted segemented and rereferenced data %%%%%%
cfg = [];
cfg.viewmode = 'vertical';
% cfg.channel = {dataV.label{1:20}}
ft_databrowser(cfg,datalap_all)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




fileoutput=[pathoutput,'DatanoArtLap_subj',num2str(subject2select),'.mat'];

save(fileoutput,'datalap_all')


% cfg=[];
% cfg.method       = 'hjorth';
% cfg.elecfile     = 'standard_1020.elc';%string, file containing the electrode definition
% cfg.elec         = lay; %structure with electrode definition
% %     %cfg.trials       = 'all' or a selection given as a 1xN vector (default = 'all')
% %     %cfg.feedback     = string, 'no', 'text', 'textbar', 'gui' (default = 'text')
% cfg.neighbours   = neighbours; %neighbourhood structure, see FT_PREPARE_NEIGHBOURS
% 
% [datalap] = ft_scalpcurrentdensity(cfg, data_all);