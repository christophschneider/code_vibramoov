
%subj=[8 9 10 11];
subj=[13 14 15 16 17 18 19 20 21 22 23 24 25];

nblocks=3;
trcode=[42 62];
laplacian = 1; %It can be 1 or 0; it is used to select unreferenced data (0) or laplacian referenced data (1)
% pathoutput='C:\Users\mdelucia\Documents\MATLAB\VibramoovProject\ProcessedData\';
pathoutput='/Users/christoph/Desktop/VibramoovAnalysis';

for j=1:length(subj)

%for j=1:1
    
subject2select=subj(j);


data_all=[];

if laplacian == 0
for kk=1:nblocks
    
    cfg=[];
    
    load([pathoutput,'DatanoArt_subj',num2str(subject2select),'_block',num2str(kk),'.mat'])
    
    if kk == 1
        data_all=data_noart;
    else
        data_all=ft_appendtimelock(cfg,data_all,data_noart);
    end
end
%

else
    
    load([pathoutput,'DatanoArtLap_subj',num2str(subject2select),'.mat'])
   
    data_all=datalap_all;
    clear datalap_all
    
end
%
%
cfg = [];
cfg.method = 'mtmfft'; %'mtmfft', analyses an entire spectrum for the entire data
                       %length, implements multitaper frequency transformation
cfg.taper = 'hanning';
cfg.output = 'pow';
cfg.foi = 1:1:40;
% cfg.toi          = -0.1980:0.05:3.0;
% cfg.t_ftimwin = -0.1980:0.05:0;
% cfg.channel=13;
cfg.keeptrials = 'yes'
cfg.trials     = find(data_all.trialinfo(:,2) == trcode(1));%'all' or a selection given as a 1xN vector (default = 'all')
freq1 = ft_freqanalysis(cfg,data_all);
cfg.keeptrials = 'yes'
cfg.trials     = find(data_all.trialinfo(:,2) == trcode(2));%'all' or a selection given as a 1xN vector (default = 'all')
freq2 = ft_freqanalysis(cfg,data_all);

% 
freq1.dimord='subj_chan_freq';
freq2.dimord=freq1.dimord
figure
ft_boundedline([],[],freq1,freq2)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')

cfg = [];
cfg.layout = 'standard_1020.elc';
cfg.showlabels = 'yes';

save([pathoutput,'psd_',num2str(subject2select)],'freq1','freq2')
% figure
% % ft_singleplotER(cfg,freq1,freq2)
% % % %
% ft_multiplotER(cfg,freq1,freq2)
% set(gca, 'YScale', 'log')
% set(gca, 'XScale', 'log')
% 

%%%%%%%%%%%%%%%%%%Rereference to average ref %%%%%%%%%%%%%%%
%     %
%     %
%     cfg.eeg             = [1:length(data_all)];
%     cfg.reref           = 'no';
%     cfg.refchannel      = cfg.eeg;%average references (only include EEG channels here)
% 
% data_reref                = ft_preprocessing(cfg,data_all);
% 
% 
% data_all=data_reref;
% clear data_reref

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg              = [];
% cfg.output       = 'pow';
% cfg.method       = 'mtmconvol';
% cfg.taper        = 'hanning';
% cfg.foi          = 1:0.5:16;                         % analysis 2 to 30 Hz in steps of 2 Hz 
% cfg.t_ftimwin    = ones(length(cfg.foi),1).*0.2;   % length of time window = 0.5 sec
% cfg.toi          = -0.1:0.1:3;                  % time window "slides" from -0.5 to 1.5 sec in steps of 0.05 sec (50 ms)
% cfg.trials     = find(data_all.trialinfo(:,2) == trcode(1));
% TFRhann1 = ft_freqanalysis(cfg, data_all);
% 
% cfg              = [];
% cfg.output       = 'pow';
% cfg.method       = 'mtmconvol';
% cfg.taper        = 'hanning';
% cfg.foi          = 1:0.5:16;                         % analysis 2 to 30 Hz in steps of 2 Hz 
% cfg.t_ftimwin    = ones(length(cfg.foi),1).*0.2;   % length of time window = 0.5 sec
% cfg.toi          = -0.1:0.1:3;                  % time window "slides" from -0.5 to 1.5 sec in steps of 0.05 sec (50 ms)
% cfg.trials     = find(data_all.trialinfo(:,2) == trcode(2));
% TFRhann2 = ft_freqanalysis(cfg, data_all);
% 
% 
% cfg=[];
% % cfg.baseline     = [-0.1 0];
% % cfg.baselinetype = 'absolute';  
% cfg.zlim         = [0 200];	        
% %cfg.ylim         = [2:10];
% cfg.showlabels   = 'yes';	
% cfg.layout       = 'standard_1020.elc';
% figure
% ft_multiplotTFR(cfg, TFRhann1);
% figure
% ft_multiplotTFR(cfg, TFRhann2);
% 
% cfg = [];
% cfg.layout       = 'standard_1020.elc';
% 
% % cfg.baseline     = [-0.1 -0];	
% % cfg.baselinetype = 'absolute';
% cfg.xlim         = [2 2.5];   
% % cfg.zlim         = [-1.5e-27 1.5e-27];
% % cfg.ylim         = [15 20];
% cfg.marker       = 'on';
% figure 
% ft_topoplotTFR(cfg, TFRhann1);
% cfg = [];
% cfg.layout       = 'standard_1020.elc';
% 
% % cfg.baseline     = [-0.1 -0];	
% % cfg.baselinetype = 'absolute';
% cfg.xlim         = [2 2.5];   
% % cfg.zlim         = [-1.5e-27 1.5e-27];
% % cfg.ylim         = [15 20];
% cfg.marker       = 'on';
% figure 
% ft_topoplotTFR(cfg, TFRhann2);
% 
% 
% 



%%%%%%%%%% Time-lock analysis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



cfg = [];
cfg.demean    = 'yes';
cfg.trials=find(data_all.trialinfo(:,2) == trcode(1));
avg_datareref1 = ft_timelockanalysis(cfg, data_all);
cfg = [];
cfg.demean    = 'yes';
cfg.trials=find(data_all.trialinfo(:,2) == trcode(2));
avg_datareref2 = ft_timelockanalysis(cfg, data_all);


cfg = [];
cfg.channel         = data_all.label;
cfg.layout          = 'standard_1020.elc';%'EEG1010';%'EEG1010';%'elec1010';
cfg.feedback        = 'no';%'yes';%'no';
lay                 = ft_prepare_layout(cfg);


% figure
cfg = [];
cfg.showlabels = 'yes';
cfg.fontsize = 6;
cfg.layout = lay;
cfg.ylim = [-10 10];
% ft_multiplotER(cfg, avg_datareref1, avg_datareref2);

% data_all1=data_all;
% data_all2=data_all;
% data_all1.trialinfo(find(data_all.trialinfo(:,2) == trcode(2)),:)=[];
% data_all2.trialinfo(find(data_all.trialinfo(:,2) == trcode(1)),:)=[];
% 
% data_all1.trial(find(data_all.trialinfo(:,2) == trcode(2)),:,:)=[];
% data_all2.trial(find(data_all.trialinfo(:,2) == trcode(1)),:,:)=[];
% 
% data_all1.dimord='subj_chan_time';
% data_all2.dimord=data_all1.dimord
% figure
% ft_boundedline([],[],data_all1,data_all2)

save([pathoutput,'avgEP_',num2str(subject2select)],'avg_datareref1','avg_datareref2')

end
