function [triggersfinal_TF]=findVibramoovNEWTriggers(trig1,tposition)

%trig1 are read from the txt file that are used to generate triggers (i.e. Protocol1_6min_gradcorrect.txt)
%tonset is the actual trigger read from the EEG data

% map stimulus timing into 500hz (500 datapoints per second)                                                                                                
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
trig1=trig1.data(:,4);   
indextrigger=find(diff(trig1) > 40); 
trig1label=trig1(indextrigger+1);
trig1label=[trig1(1); trig1label];

triggersfinal_TF=zeros(1,tposition(end));

triggersfinal_TF(tposition)=trig1label';