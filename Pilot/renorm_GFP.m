function [S_norm,GFPscaling] = renorm_GFP(S, base, epoch)

%RENORM_GFP Performs Global Field Power normalization.
%   [SN] = RENORM_GRP(S,EPOCH) returns tha M x N matrix of
%   the S EEG data, after subtracting at each time-point
%   tha instataneous GFP. EPOCH is the trials' length in TF.
%
%   [SN,GFP_SCALING] = RENORM_GRP(S,EPOCH) also returns
%   the instataneous GFP values.
%
%   See also prepross_GFP.

%compute average reference:
avg_ref = sum(S,2)/size(S,2);
for i =1:size(S,2)
    S(:,i) = S(:,i)-avg_ref;
end
clear avg_ref

%Compute the GFP:
seeg=S.*S;
GFPscaling=squeeze(sqrt(sum(seeg,2)/size(seeg,2)));


%GFP normalization:
S_norm = zeros(size(S));
for i=1:size(S,2)
   S_norm(:,i)=squeeze(S(:,i))./GFPscaling;    
end

clear S

if ~isempty(base)
    for i = 1:epoch:size(S_norm, 1)
        S_norm(i:i+epoch-1,:) = S_norm(i:i+epoch-1,:) - repmat(mean(S_norm(i:i+base-1,:)), [epoch 1]);
    end  
    
end

