% author: Christoph Schneider, CHUV, 2019

% Defines the default values for the EEG Vibramoov pilot project trigger fix.
% +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%% GENERAL

% Path to third party dependencies
addpath(genpath('/Users/christoph/Documents/Git/Code_2019_MATLAB-Vibramoov/Final/Third_Party_Dependencies'))

% Path to the raw data files
default.path.rawdata = '/Users/christoph/Desktop/CHUV/Vibramoov/Data_Raw';

% Path to the Vibramoov sequence files
default.path.sequences = '/Users/christoph/Desktop/CHUV/Vibramoov/Sequences';

% Identifier for session folders
default.sessionstring = 'ses';
