% author: Christoph Schneider, CHUV, 2019

% Offline analysis of Vibramoov pilot data for experiment
% Start this file to start processing - all the steps are implemented in a
% modular way and can therefore be reordered and/or commented out

% Fix the triggers and put them in an easier usable format

clear;
close all force;
clc;

% Load defaults
% -------------------------------------------------------------------------
Trigger_Defaults()

% Select folders
% -------------------------------------------------------------------------
cd(default.path.rawdata);
folders = uipickfiles('Prompt','Select Folders');

for sub = 1:size(folders,2)
    disp(['++++ Processing folder ',num2str(sub),' of ',num2str(size(folders,2)),' ++++']);
    
    % find sub-folders which correspond to sessions
    args.pathname = folders{sub};
    session = dir(fullfile(folders{sub},['*',default.sessionstring,'*']));
    
    % iterate over sessions
    for r = 1:size({session},2)
        
        sesname = {session.name}; sesname = sesname{r};
        % get list of eeg recording files in session folder
        eegfiles = dir(fullfile(folders{sub},session.name,'eeg','*.*df'));
        args.filenames = eegfiles;
        
        % call analysis function
        Trigger_Main(args);
        
    end
end

disp('---> Batch file processing end <---')