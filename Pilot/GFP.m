% This script contains Global field power GFP batch analysis for 
% the Vibramoov preprocessed data

% Vibramoov pilot
% Christoph Schneider
% CHUV, 2019
% -------------------------------------------------------------------------

clear;

clc;
close all;

args.path.source = '/Users/christoph/Desktop/Vibramoov/Data_Preprocessed';
args.path.save = '/Users/christoph/Desktop/Vibramoov/Results';
args.spatialfilter = 'no';
args.spectralfilter = 'no';
args.filter.high = 30;
args.filter.low = 1;
args.baseline = 'yes';
args.baselinetime = [-1.5 -1];

cd(args.path.source);
files = uipickfiles('Prompt','Select Folders');

for sub = 1:size(files,2)
    disp(['++++ Processing file ',num2str(sub),' of ',num2str(size(files,2)),' ++++']);
    args.filename = files{sub};
    args.sub = sub;
    
    % call analysis function
    [avg(sub), trialavg(sub)] = ERP_body(args);
    illusion(sub,:) = avg(sub).illusion.avg;
    control(sub,:) = avg(sub).control.avg;
end

figure;
plot(illusion','r')
hold on;
plot(control','b')

%% Make grand averages

cfg = [];
grandavg.illusion = ft_timelockgrandaverage(cfg, avg(:).illusion);
cfg = [];
grandavg.control = ft_timelockgrandaverage(cfg, avg(:).control);

cfg = [];
cfg.layout = 'elec1010.lay';
cfg.interactive = 'yes';
cfg.showoutline = 'yes';
ft_singleplotER(cfg, grandavg.illusion, grandavg.control)

cfg = [];
cfg.operation = 'subtract';
cfg.parameter = 'avg';

grandavg.difference = ft_math(cfg, grandavg.illusion, grandavg.control);

figure;
cfg.layout = 'elec1010.lay';
cfg.interactive = 'yes';
cfg.showoutline = 'yes';
ft_multiplotER(cfg, grandavg.difference)

%% build neighbors structure
cfg = [];
cfg.channel       = grandavg.illusion.label;
cfg.template      = 'elec1010_neighb.mat';
cfg.elec          = 'standard_1005.elc';
cfg.method        = 'distance';
cfg.neighbourdist = 50;

nb = ft_prepare_neighbours(cfg);

cfg = [];
cfg.channel = 'eeg';
cfg.latency = 'all';

cfg.method = 'montecarlo';
cfg.statistic = 'depsamplesT';
cfg.correctm = 'cluster';
cfg.clusteralpha = 0.105;
cfg.clusterstatistic = 'maxsum';
cfg.minnbchan = 1;
cfg.neighbours       = nb;
cfg.tail = 0;
cfg.clustertail = 0;
cfg.alpha = 0.025;
cfg.numrandomization = 500;

subj = numel(avg);
design = zeros(2,2*subj);
for i = 1:subj
    design(1,i) = i;
end
for i = 1:subj
    design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;
cfg.spmversion = 'spm12';

stat = ft_timelockstatistics(cfg, avg.illusion, avg.control);

%% --- plot

pos_signif_clust = find([stat.posclusters.prob] < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);

neg_signif_clust = find([stat.negclusters.prob] < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, neg_signif_clust);

cfg = [];
cfg.layout	= 'elec1010.lay';
cfg.marker  = 'labels';

ft_topoplotER(cfg, grandavg.difference);

%% statistics across trials (channel-wise)

for k = avg(1).illusion.label(:)'
    
    cfg = [];
    cfg.channel = k{1};
    cfg.latency = 'all';
    
    cfg.method = 'montecarlo';
    cfg.statistic = 'depsamplesT';
    cfg.correctm = 'cluster';
    cfg.clusteralpha = 0.105;
    cfg.clusterstatistic = 'maxsum';
    cfg.minnbchan = 1;
    cfg.neighbours       = nb;
    cfg.tail = 0;
    cfg.clustertail = 0;
    cfg.alpha = 0.025;
    cfg.numrandomization = 500;
    
    subj = numel(avg);
    design = zeros(2,2*subj);
    for i = 1:subj
        design(1,i) = i;
    end
    for i = 1:subj
        design(1,subj+i) = i;
    end
    design(2,1:subj)        = 1;
    design(2,subj+1:2*subj) = 2;
    
    cfg.design = design;
    cfg.uvar  = 1;
    cfg.ivar  = 2;
    cfg.spmversion = 'spm12';
    
    statx = ft_timelockstatistics(cfg, avg.illusion, avg.control);
    proby(ismember(avg(1).illusion.label,k),:) = statx.prob;
end


a = struct2cell(trialavg);
b = reshape(cell2mat(a(3,:)),size(trialavg(1).channelstat,1),size(trialavg(1).channelstat,2),[]);
figure;
plot(mean(b(1,:,:),3)');

%% ************************************************************************
%% ************************************************************************
%% ************************************************************************



function [task, trials] = ERP_body(args)

%% load data

% loads it to the variable 'data_preprocessed'
load(args.filename);

%% spatial filtering

if strcmp(args.spatialfilter,'yes')
    cfg = [];
    cfg.method      = 'spline';
    cfg.elec        = 'standard_1005.elc';
    cfg.trials      = 'all';
    cfg.feedback    = 'no';
    data_preprocessed    = ft_scalpcurrentdensity(cfg, data_preprocessed);
end

%% spectral filtering

if strcmp(args.spectralfilter,'yes')
    
    cfg=[];
    cfg.continuous   = 'yes';
    cfg.channel       = 'eeg';
    cfg.padding       = 10;
    cfg.reref         = 'no';
    cfg.bpfilter      = 'yes';
    cfg.bpfreq        = [args.filter.low args.filter.high];
    cfg.bpfiltord     = 4;
    cfg.bpfilttype    = 'but';
    cfg.hpfiltdir     = 'twopass';
    
    data_preprocessed = ft_preprocessing(cfg,data_preprocessed);
end

%% compute GFP

data_gfp = data_preprocessed;
data_gfp.label = {'gfp'};
for tr = 1:length(data_gfp.trial)
data_gfp.trial{tr} = nanstd(data_gfp.trial{tr});
end

data_preprocessed = data_gfp;
%% ERP

% use ft_timelockanalysis to compute the ERPs
cfg = [];
cfg.trials = find(data_preprocessed.trialinfo==90);
cfg.channel = 'gfp';
cfg.removemean = 'yes';
task.illusion = ft_timelockanalysis(cfg, data_preprocessed);
cfg.keeptrials = 'yes';
trials.illusion = ft_timelockanalysis(cfg, data_preprocessed);

cfg = [];
cfg.channel = 'gfp';
cfg.removemean = 'yes';
cfg.trials = find(data_preprocessed.trialinfo==70);
task.control = ft_timelockanalysis(cfg, data_preprocessed);
cfg.keeptrials = 'yes';
trials.control = ft_timelockanalysis(cfg, data_preprocessed);


if strcmp(args.baseline,'yes')
    
    cfg = [];
    cfg.baseline = args.baselinetime;
    cfg.channel = 'gfp';
    
    task.illusion = ft_timelockbaseline(cfg, task.illusion);
    task.control = ft_timelockbaseline(cfg, task.control);
    
end

figure;
plot(task.illusion.avg);
hold on;
plot(task.control.avg);

% cfg = [];
% cfg.layout = 'elec1010.lay'; %'mpi_customized_acticap64.mat';
% cfg.interactive = 'yes';
% cfg.showoutline = 'yes';
% ft_multiplotER(cfg, task.illusion, task.control)

%% difference wave
cfg = [];
cfg.operation = 'subtract';
cfg.parameter = 'avg';
difference = ft_math(cfg, task.illusion, task.control);
%
% % note that the following appears to do the sam
% % difference     = task1;                   % copy one of the structures
% % difference.avg = task1.avg - task2.avg;   % compute the difference ERP
% % however that will not keep provenance information, whereas ft_math will
%
% cfg = [];
% cfg.layout      = 'elec1010.lay'; %'mpi_customized_acticap64.mat';
% cfg.interactive = 'yes';
% cfg.showoutline = 'yes';
% ft_multiplotER(cfg, difference);



% %% build neighbors structure
% cfg = [];
% cfg.channel       = task.illusion.label;
% cfg.template      = 'elec1010_neighb.mat';
% cfg.elec          = 'standard_1005.elc';
% cfg.method        = 'distance';
% cfg.neighbourdist = 50;
% 
% 
% cfg.method   = 'triangulation';
% cfg.feedback = 'yes'; % visualizes the neighbors
% 
% nb = ft_prepare_neighbours(cfg);


% %% statistics across channels
% cfg = [];
% cfg.channel = 'gfp';
% cfg.latency = 'all';
% 
% cfg.method = 'montecarlo';
% cfg.statistic = 'depsamplesT';
% cfg.correctm = 'cluster';
% cfg.clusteralpha = 0.05;
% cfg.clusterstatistic = 'maxsum';
% cfg.minnbchan = 1;
% % cfg.neighbours       = nb;
% cfg.tail = 0;
% cfg.clustertail = 0;
% cfg.alpha = 0.025;
% cfg.numrandomization = 500;
% 
% subj = 1;
% design = zeros(2,2*subj);
% for i = 1:subj
%     design(1,i) = i;
% end
% for i = 1:subj
%     design(1,subj+i) = i;
% end
% design(2,1:subj)        = 1;
% design(2,subj+1:2*subj) = 2;
% 
% cfg.design = design;
% cfg.uvar  = 1;
% cfg.ivar  = 2;
% cfg.spmversion = 'spm12';
% 
% stat = ft_timelockstatistics(cfg, task.illusion, task.control);

%% statistics across trials (channel-wise)

% for k = trials.illusion.label(:)'
    
    design = (data_preprocessed.trialinfo > 80)+1;
    cfg           = [];
    cfg.channel     = 'gfp';
    
    cfg.method           = 'montecarlo'; % use montecarlo to permute the data
    cfg.statistic        = 'ft_statfun_indepsamplesT'; % function to use when ...
    % calculating the ...
    % parametric t-values
    
    cfg.correctm         = 'cluster'; % the correction to use
    cfg.clusteralpha     = 0.05; % the alpha level used to determine whether or ...
    % not a channel/time pair can be included in a ...
    % cluster
    cfg.alpha            = 0.025; % corresponds to an alpha level of 0.05, since ...
    % two tests are made ...
    % (negative and positive: 2*0.025=0.05)
    cfg.numrandomization = 100;  % number of permutations run
%     cfg.neighbours       = nb;
    cfg.spmversion = 'spm12';
    
    
    % cfg.method    = 'analytic'; % using a paarmetric test
    % cfg.statistic = 'ft_statfun_indepsamplesT'; % using independent samples
    % cfg.correctm  = 'no'; % no multiple comparisons correction
    % cfg.alpha     = 0.05;
    % cfg.correctm  = 'fdr';
    % cfg.latency     = [0 2];
    
    cfg.design    = design; % indicating which trials belong ...
    % to what category
    cfg.ivar      = 1; % indicating that the independent variable is found in ...
    % first row of cfg.design
    
    stat = ft_timelockstatistics(cfg, trials.illusion, trials.control);
    
% end

figure;
plot(stat.prob)

close all

%% ---



end