close all;
clear;
clc;

V_defaults;

% ---------- change for batch processing
cd(default.path.data);
datafilename = default.datafile;

subj = '01';
sess = '01';
% ----------


% cfg = [];
% cfg.dataset = datafilename;
% cfg.trialdef.eventtype  = 'STATUS';
% cfg.trialdef.eventvalue = default.events.values;
% cfg.trialdef.prestim    = default.trial.time.pre;
% cfg.trialdef.poststim   = default.trial.time.post;
% cfg = ft_definetrial(cfg);

for block = 1:3
    
    % create the trial definition
    cfg=[];
    cfg.dataset             = ['sub-',subj,'_ses-',sess,'_task-fps_run-',num2str(block,'%02.f'),'_eeg.bdf'];
    cfg.trialdef.eventtype  = 'STATUS';
    cfg.trialdef.eventvalue = default.events.values;
    cfg.trialdef.prestim    = default.trial.time.pre;
    cfg.trialdef.poststim   = default.trial.time.post;
    cfg = ft_definetrial(cfg);
    
    % remember condition and/or block
    trl = cfg.trl;
    trl(:,8) = block;
    
    % define the relevant trials
    for i=1:length(trl)
        if ismember(trl(i,4), [90 82])
            trl(i,7) = 1; % illusion
        elseif ismember(trl(i,4), [70 62])
            trl(i,7) = 2; % non-illusion
        end
    end
    
    % read and preprocess the data using the trial definition
    cfg=[];
    cfg.dataset       = ['sub-',subj,'_ses-',sess,'_task-fps_run-',num2str(block,'%02.f'),'_eeg.bdf'];
    cfg.channel       = 'eeg';
    cfg.trl           = trl;
    cfg.padding       = 1;
    cfg.reref         = 'no';
    
    data = ft_preprocessing(cfg);
    
    % save the data to disk
    save(fullfile(default.path.save,['sub-',subj,'_ses-',sess,'_task-fps_run-',num2str(block,'%02.f'),'_eeg.bdf']), 'data')
end


clearvars -except default subj sess datafilename

% concatenate all trials/blocks
ipart=1;
for block = 1:3
    disp(['Loading ','sub-',subj,'_ses-',sess,'_task-fps_run-',num2str(block,'%02.f'),'_eeg.bdf']);
    load(['sub-',subj,'_ses-',sess,'_task-fps_run-',num2str(block,'%02.f'),'_eeg.bdf'], 'data');
    datapart(ipart) = data;
    ipart=ipart+1;
end

cfg=[];
data = ft_appenddata(cfg, datapart(1), datapart(2),datapart(3),datapart(4),datapart(5),datapart(6),datapart(7),datapart(8),datapart(9),datapart(10),datapart(11),datapart(12),datapart(13),datapart(14),datapart(15),datapart(16));
clear datapart*

% detect eog artifacts using ICA
cfg=[];
cfg.method  = 'runica';
cfg.channel = 1:160; % EEG channels only
datacomp = ft_componentanalysis(cfg, data);
save('analysis/ica/datacomp', 'datacomp')

% plot the components to detect the artifacts
figure
k=1; f=1;
for icomp=1:length(datacomp.topo)
    if k>20
        k=1;
        figure
    end
    cfg=[];
    cfg.layout = 'biosemi160.lay';
    cfg.xlim   = [icomp icomp];
    
    subplot(4,5,k);
    ft_topoplotER(cfg, datacomp);
    title(icomp);
    
    k = k+1;
end

% remove components that reflect eog artifacts
cfg=[];
cfg.component = [12 49]; % note the exact numbers will vary per run
data = ft_rejectcomponent(cfg, datacomp);
save('analysis/data_clean', 'data')

%% manual artifact rejection

% remove the mean
cfg=[];
cfg.demean = 'yes';
data = ft_preprocessing(cfg, data);

% shuffle the trials for non-biased artifact rejection
shuffle = randperm(length(data.trial));
datashuff = data;
for i=1:length(data.trial)
    datashuff.trial{i} = data.trial{shuffle(i)};
    datashuff.time{i}  = data.time{shuffle(i)};
    datashuff.trialinfo(i,:) = data.trialinfo(shuffle(i),:);
end

% browse for artifact rejection
cfg = [];
cfg.channel = 'EEG';
cfg.continuous = 'no';
cfg = ft_databrowser(cfg, data);
data = ft_rejectartifact(cfg,data);

% visual artifact rejection in summary mode
cfg = [];
cfg.method = 'summary';
data = ft_rejectvisual(cfg, data);

% the following trials were removed: 43, 87, 174, 261, 307, 333, 343, 398, 557, 583, 593, 606, 666, 670, 674, 741, 742, 743, 744, 745, 899
% the following channels were removed: A7, A13, A14, A25, A26, A27, A28, B8, B9, B12, B19, B32, D1, D21, D23, D25, D26, D31, D32, E1, E17, E21, E32

save('analysis/data_clean', 'data')

