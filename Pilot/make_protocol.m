% make Vibramoov files

clear;
clc;
close all;


numberTrials = 30;
illusion_ratio = .5; % in percent of total number of trials (above)
Trial_duration = 3; % in seconds
Intertrial_duration = [10,10]; % in seconds
discrete = 'yes'; % trial/intertrial durations in full seconds or not
total_lines = 12;
sfq = 5; % sampling frequency in Hz

%% **************

numberIntertrials = numberTrials;

if strcmp(discrete,'yes')
    iTt = datasample(Intertrial_duration(1):Intertrial_duration(2),numberIntertrials);
end

Tt = ones(1,numberTrials) .* Trial_duration;

% ask for felt extension / flexion / nothing

flexion = [[42:2:50, repmat(50,1,(Trial_duration - 2)*sfq) ,50:-2:42]',...
    [82:2:90, repmat(90,1,(Trial_duration - 2)*sfq) ,90:-2:82]'];

extension = [[82:2:90, repmat(90,1,(Trial_duration - 2)*sfq) ,90:-2:82]',...
    [42:2:50, repmat(50,1,(Trial_duration - 2)*sfq) ,50:-2:42]'];

sham = [[62:2:70, repmat(70,1,(Trial_duration - 2)*sfq) ,70:-2:62]',...
    [62:2:70, repmat(70,1,(Trial_duration - 2)*sfq) ,70:-2:62]'];

% illusion = Tt/10;
% illusion(1:length(illusion)/2) = illusion(1:length(illusion)/2) + 1;
% illusion(randperm(length(illusion)));

illusion = sort(randperm(numberTrials, round(numberTrials*illusion_ratio)));


%% Build table

T = zeros(10,total_lines);

for iT = 1:numberTrials
    clear Tx Ty
    for l = 1:total_lines

        % add trial
        Tx(:,l) = zeros(Tt(iT) * sfq,1);
        
        % add intertrial
        Ty(:,l) = zeros(iTt(iT) * sfq,1);
    end
    if ismember(iT, illusion)
        Tx(:,4:5) = sham;
    else 
        Tx(:,4:5) = flexion;
    end
    
    T = [T; Tx ; Ty];
end


% initialize table
varNames = {'Out_1','Out_2','Out_3','Out_4','Out_5','Out_6','Out_7',...
    'Out_8','Out_9','Out_10','Out_11','Out_12'};
varNames2 = {'[Out_1]','[Out_2]','[Out_3]','[Out_4]','[Out_5]','[Out_6]','[Out_7]',...
    '[Out_8]','[Out_9]','[Out_10]','[Out_11]','[Out_12]'};
T = array2table(T);
T.Properties.VariableNames = varNames;

writetable(T,'behavioral_Block3.txt','Delimiter','\t')
