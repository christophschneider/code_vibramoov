function [] = plotclassification(time, data, labels, k)

% plotpos = [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
% elpos = {'Fz', 'FC3', 'FC2', 'FCz', 'FC2', 'FC4', 'C3', 'C1', 'Cz', 'C2' ,'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};
% time = trials(1).illusion.time + 1;
% yl = [-12, 12];     % y-axis figure limits

figure;
% subplot(2,1,1)
cmap = colormap('lines');

% ttl = ['Training trial averages illusion and control for outer fold ',num2str(k)];
ptx.illusion = data.train(labels.train==90,:);
ptx.control = data.train(labels.train==70,:);
semdim.illusion = size(ptx.illusion,1);
semdim.control = size(ptx.control,1);

% perform test
[ signif,fpos,nsignif ] = permstattest({ptx.illusion,ptx.control},1000,0.05,0.05,'ttest2',0,3);
% extract cluster positions
clusterpos = [find( diff([0;signif]) == 1), find( diff([signif;0]) == -1)];
clusterposx = [find( diff([0;fpos]) == 1), find( diff([fpos;0]) == -1)];



% plot channel mean
boundedline(time,squeeze(mean(ptx.illusion,1)),squeeze(std(ptx.illusion,[],1))/sqrt(semdim.illusion) ,'alpha', 'cmap', cmap(1,:));
hold on;
boundedline(time,squeeze(mean(ptx.control,1)),squeeze(std(ptx.control,[],1))/sqrt(semdim.control) ,'alpha', 'cmap', cmap(2,:));
vline(0,'k');
vline(1,'k:');
vline(2,'k:');
vline(3,'k');
xlabel('Time [s]')
ylabel('Amplitude [�V]')
title('channel mean')
xlim([time(1), time(end)])
% ylim(yl)
set(gca, 'YDir','reverse')

%----------------

cmap = colormap('lines');


ttl = ['Test trial averages illusion and control for outer fold ',num2str(k)];
pty.illusion = data.test(labels.test==90,:);
pty.control = data.test(labels.test==70,:);

% plot channel mean
plot(time, squeeze(mean(pty.illusion,1)),'--');
plot(time, squeeze(mean(pty.control,1)),'--');

yl = ylim;
% draw gray rectangel to highlight the significant cluster permutation part
for cl = 1:size(clusterpos,1)
    rectangle('Position',[time(clusterpos(cl,1)),yl(1),time(clusterpos(cl,2)) - time(clusterpos(cl,1)),yl(2)-yl(1)],...
        'FaceColor', [0, 0, 0, 0.1], ...
        'EdgeColor', [0, 0, 0, 0.1]);
end
for cl = 1:size(clusterposx,1)
    rectangle('Position',[time(clusterposx(cl,1)),yl(1),time(clusterposx(cl,2)) - time(clusterposx(cl,1)),yl(2)-yl(1)],...
        'FaceColor', [0, 0, 0, 0.2], ...
        'EdgeColor', [0, 0, 0, 0.2]);
end

% plot title
legend({'illusion train','','control train','','illusion test','control test'})
title(ttl)
